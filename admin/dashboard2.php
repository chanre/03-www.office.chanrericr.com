<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$RF_coded=$_POST['RF_coded'];
$ANA=$_POST['ANA'];
$TAL=$_POST['TAL'];
$Comorbidities=$_POST['Comorbidities'];
$TJC_B=$_POST['TJC_B'];
$CRP_B=$_POST['CRP_B'];
$ESR_B=$_POST['ESR_B'];
$N_B=$_POST['N_B'];
$L_B=$_POST['L_B'];
$NLR_B=$_POST['NLR_B'];
$DAS_B=$_POST['DAS_B'];
$single_combination=$_POST['single_combination'];
$code_steriods=$_POST['code_steriods'];
$age=$_POST['age'];
$Gender=$_POST['Gender'];
$duration_symptoms=$_POST['duration_symptoms'];
$patient_id=$_POST['patient_id'];

$query=mysqli_query($con,"insert into rdata(RF_coded,ANA,TAL,Comorbidities,TJC_B,CRP_B,ESR_B,N_B,L_B,NLR_B,DAS_B,single_combination,code_steriods,age,Gender,duration_symptoms,patient_id) values('$RF_coded','$ANA','$TAL','$Comorbidities','$TJC_B','$CRP_B','$ESR_B','$N_B','$L_B','$NLR_B','$DAS_B','$single_combination','$code_steriods','$age','$Gender','$duration_symptoms','$patient_id')");
if($query)
    {$result="Successfully Submitted!!!";}
else{$result="Failed.. Please Try Again.";}
}
?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title></title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
</style>
</head> 

<body>    
    <?php include('layout/header.php');?>
    <div class="container-fluid">	
    <div class="row">	
    		<div class="col-md-2 check">
                <br>
                <ul >
                <li>Dashboard</li>
                    <li>Result</li>
                </ul>
            </div>
    		<div class="col-md-6 check">
                <br>
            <form name="addpost" method="post" enctype="multipart/form-data">
            <div class="">
            <h3 class="m-t-10 m-b-10" align="center" style="background-color: #eee;width: 100%;box-shadow: 10px;font-family: times;line-height: 50px;"><b>INSERTION FORM</b></h3>
           </div>
            <table>
                 <tr>
                    <td>Patient Id</td>
                    <td><input type="text" class="" id="patient_id" name="patient_id"  required></td>
                </tr>
                <tr>
                    <td>RF_coded</td>
                    <td><input type="text" class="" id="RF_coded" name="RF_coded"  required></td>
                </tr>
                <tr>
                    <td>ANA</td>
                    <td><input type="text" class="" id="ANA" name="ANA"  required></td>
                </tr>
                <tr>
                    <td>Time to Achive LDA < 2.6 in months</td>
                    <td><input type="text" class="" id="TAL" name="TAL"  required></td>
                </tr>
                <tr>
                    <td>Comorbidities</td>
                    <td><input type="text" class="" id="Comorbidities" name="Comorbidities"  required></td>
                </tr>
                <tr>
                    <td>TJC_B</td>
                    <td><input type="text" class="" id="TJC_B" name="TJC_B"  required></td>
                </tr>
                <tr>
                    <td>SJC_B</td>
                    <td><input type="text" class="" id="SJC_B" name="SJC_B"  required></td>
                </tr>
                <tr>
                    <td>CRP_B</td>
                    <td><input type="text" class="" id="CRP_B" name="CRP_B"  required></td>
                </tr>
                <tr>
                    <td>ESR_B</td>
                    <td><input type="text" class="" id="ESR_B" name="ESR_B"  required></td>
                </tr>
                <tr>
                    <td>N_B</td>
                    <td><input type="text" class="" id="N_B" name="N_B"  required></td>
                </tr>
                <tr>
                    <td>L_B</td>
                    <td><input type="text" class="" id="L_B" name="L_B"  required></td>
                </tr>
                <tr>
                    <td>NLR_B</td>
                    <td><input type="text" class="" id="NLR_B" name="NLR_B"  required></td>
                </tr>
                <tr>
                    <td>DAS_B</td>
                    <td><input type="text" class="" id="DAS_B" name="DAS_B"  required></td>
                </tr>
                <tr>
                    <td>code_ DMARDs</td>
                    <td><input type="text" class="" id="code_DMARDs" name="code_DMARDs"  required></td>
                </tr>
                <tr>
                    <td>single_combination</td>
                    <td><input type="text" class="" id="single_combination" name="single_combination"  required></td>
                </tr>
                <tr>
                    <td>code_steriods</td>
                    <td><input type="text" class="" id="code_steriods" name="code_steriods"  required></td>
                </tr>
                <tr>
                    <td>age</td>
                    <td><input type="text" class="" id="age" name="age"  required></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td><input type="text" class="" id="Gender" name="Gender"  required></td>
                </tr>
                <tr>
                    <td>duration_symptoms</td>
                    <td><input type="text" class="" id="duration_symptoms" name="duration_symptoms"  required></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table
<br>
<div align="center">
<button type="submit" name="submit"  class="btn btn-success waves-effect waves-light">Check</button>
 <button type="button" class="btn btn-danger waves-effect waves-light">Discard</button>
</div><br>

                                        </form> <br>
    		</div>
    		<div class="col-md-4 check">
            <table class="table dt-table">
									<thead>
									<tr>
                                    <th>
											PId
										</th>
										<th>
                                        RF_coded
										</th>
										
										<th>
                                        Result
										</th>
										<th>
											Action
										</th>
										
									</tr>
									</thead>
									<tfoot>
									<tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
									<th></th>
									</tr>
									</tfoot>
                                    <tbody>
              <?php 
                $query1=mysqli_query($con,"Select * from rdata");
while($rows=mysqli_fetch_array($query1))
{
                
               ?>                    
			<tr>
				<td><?php echo htmlentities($rows['patient_id']); ?></td>
				<td><?php echo htmlentities($rows['RF_coded']); ?></td>
				<td><?php echo htmlentities($rows['results']); ?></td>
				<td><a href="" class="btn btn-info"><i class="fa fa-info-circle" aria-hidden="true"></i></a></td>
			
			</tr>
            <?php } ?>
                                    </tbody>
									</table>
            </div>

    </div>	
    </div>	
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h1 class="page-heading single-col-max mx-auto"></h1>
		    <div class="page-intro single-col-max mx-auto"></div>
		    <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
	    </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h3 class="mb-2 text-white mb-3"></h3>
		    <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
		    <div class="pt-3 text-center">
			   
		    </div>
	    </div>
    </section>

               
    
 <?php include('layout/footer.php');?>

    <!-- Javascript -->          
    <script src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/js/function.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
<script>
            $(document).on("click", "#save", function () {
                //get value of message 
                var patient_id = $("#patient_id").val();
                var RF_coded = $("#RF_coded").val();
                var ANA = $("#ANA").val();
                var TAL = $("#TAL").val();
                var Comorbidities = $("#Comorbidities").val();
                var TJC_B = $("#TJC_B").val();
                var CRP_B = $("#CRP_B").val();
                var ESR_B = $("#ESR_B").val();
                var N_B = $("#N_B").val();
                var L_B = $("#L_B").val();
                var NLR_B = $("#NLR_B").val();
                var DAS_B = $("#DAS_B").val();
                var code_steriods = $("#code_steriods").val();
                var age = $("#age").val();
                var Gender = $("#Gender").val();
                var duration_symptoms = $("#duration_symptoms").val();
               

                //check if value is not empty
              
                //Ajax call to send data to the insert.php
                $.ajax({
                    type: "POST",
                    url: "insert.php",
                    data: {
                        patient_id : patient_id,
                        RF_coded:RF_coded,
                        ANA:ANA,TAL:TAL,Comorbidities:Comorbidities,TJC_B:TJC_B,CRP_B:CRP_B,ESR_B:ESR_B,N_B:N_B, 
                        L_B:L_B,NLR_B:NLR_B,DAS_B:DAS_B,code_steriods:code_steriods,age:age,Gender:Gender,duration_symptoms:duration_symptoms

                        

                    },
                    cache: false,
                    success: function (data) {
                        //Insert data before the message wrap div
                        $(data).insertBefore(".message-wrap:first");
                        //Clear the textarea message
                        $("#message").val("");
                    }
                });
            });
        </script>
</body>
</html> 

<?php } ?>
<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title></title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;;border: 2px solid #ffffff;}
    table{width: 100%;background: white;text-align: center;}
</style>
</head> 

<body>    
    <?php include('layout/dasheader.php');?>
    <div class="container-fluid check"> <br>
    <div class="row ">  
            <div class="col-md-3">
                 <?php include ('layout/sidebar.php'); ?>
            </div>
            <div class="col-md-9">
                <table border="2">
                    <tr>
                        <th>Patient ID</th>
                        <th>RF_coded</th>
                        <th>Age</th>
                        <th>Gender</th>
                         <th>Patient ID</th>
                        <th>Action</th>
                    </tr>
                    <?php $myquery=mysqli_query($con,"Select * from das_table where is_active=1");
                    while ($rows = mysqli_fetch_array($myquery)) {
                        
                   
                     ?>
                    <tr>
                        <td><?php echo htmlentities($rows['patient_id']) ?></td>
                        <td><?php echo htmlentities($rows['RF']) ?></td>
                        <td><?php echo htmlentities($rows['Age']) ?></td>
                        <td><?php echo htmlentities($rows['Gender']) ?></td>
                       
                        <td><a href="dasedit.php?pid=<?php echo htmlentities($rows['id']);?>" class="btn btn-success">Add Results</a><a href="" class="btn btn-danger">Delete</a></td>
                    </tr>
                    <?php  } ?>
                </table>
            </div>
            
           
    </div>  
<br>
</div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
           
            <div class="pt-3 text-center">
               
            </div>
        </div>
    </section>

               
    
 <?php include('layout/footer.php');?>
       
    <!-- Javascript -->          
    <script src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/js/function.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

</body>
</html> 

<?php } ?>
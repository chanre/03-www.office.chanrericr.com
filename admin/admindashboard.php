<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title></title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
	.side-item{
		background-color: white;
		margin: 30px;
		padding: 10px;
	}
	.side-item:hover{
		background-color: black;
		color:white;
		margin: 30px;
		padding: 10px;
	}
	.side-item a{
		width: 100%;
	}
	ul{

	}
</style>
</head> 

<body>    
   <header class="header fixed-top">	    
        <div class="branding docs-branding">
            <div class="container-fluid position-relative py-2">
                <div class="docs-logo-wrapper">
	                <div class="site-logo"><a class="navbar-brand" href="admindashboard.php"></a></div>    
                </div>
	            <div class="docs-top-utilities d-flex justify-content-end align-items-center">
	
					<ul class="social-list list-inline mx-md-3 mx-lg-5  d-none d-lg-flex">
						<li class="list-inline-item"><div class="dropdown">
  <button class="bg-outline-primary dropdown-toggle" type="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   <?php echo $_SESSION['login']; ?>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="logout.php">Sign Out</a>
    <a class="dropdown-item" href="changepassword.php">Change Password</a>
   
  </div>
</div></li>
<li class="list-inline-item"><small><a href=""></a></small></li>
		            </ul>
		           
	            </div>
            </div>
        </div>
    </header>
	
	
	
    <div class="container-fluid">	
    <div class="row">	
    		<div class="col-md-3 check">
                <br>
    <div align="center">
				<img src="images/nopics.jpg" width="200" height="200">
				</div>
				<br>
            </div>
    		<div class="col-md-3 check">
              <div class="side-item">
		<a href="/admin/dashboard.php"  class="btn btn-primary">Remission Data</a>
	</div>
	
	<div class="side-item">
		<a href="https://office.chanrericr.com/admin/dasdata.php"  class="btn btn-primary">Das Data</a>
	</div>
    		</div>
			<div class="col-md-3 check">
              <div class="side-item">
		<a href="infectiondata.php"  class="btn btn-primary">Infection Data</a>
	</div>
	
	<div class="side-item">
		<a href="https://office.chanrericr.com/admin/message.php"  class="btn btn-primary">Message</a>
	</div>
    		</div>
    		<div class="col-md-3 check">
					<div class="side-item">
						  <a href="https://office.chanrericr.com/board/admin_add_article.php"  class="btn btn-primary">Posts And Articles</a>
				   </div>
            </div>

    </div>	
    </div>	
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h1 class="page-heading single-col-max mx-auto"></h1>
		    <div class="page-intro single-col-max mx-auto"></div>
		    <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
	    </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h3 class="mb-2 text-white mb-3"></h3>
		    <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
		    <div class="pt-3 text-center">
			   
		    </div>
	    </div>
    </section>

               
    
 <?php include('layout/footer.php');?>

    <!-- Javascript -->          
    <script src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/js/function.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

</body>
</html> 

<?php } ?>
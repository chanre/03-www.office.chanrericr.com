<div class="sidemenu">
	<div class="side-item">
		<a href="https://office.chanrericr.com/admin/admindashboard.php" class="btn btn-primary">Main Dashboard</a>
	</div>

	<div class="side-item">
		<a href="https://office.chanrericr.com/admin/add-user.php"  class="btn btn-primary">Add User</a>
	</div>
	
	<div class="side-item">
		<a href="https://office.chanrericr.com/board/admin_read_article.php"  class="btn btn-primary">Read Article</a>
	</div>
</div>
<style type="text/css">
	.side-item{
		background-color: white;
		margin: 2px;
		padding: 10px;
	}
	.side-item:hover{
		background-color: black;
		color:white;
		margin: 2px;
		padding: 10px;
	}
	.side-item a{
		width: 100%;
	}
</style>
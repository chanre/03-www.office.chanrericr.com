<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$RF_coded=$_POST['RF_coded'];
$ANA=$_POST['ANA'];
$TAL=$_POST['TAL'];
$Comorbidities=$_POST['Comorbidities'];
$TJC_B=$_POST['TJC_B'];
$CRP_B=$_POST['CRP_B'];
$ESR_B=$_POST['ESR_B'];
$N_B=$_POST['N_B'];
$L_B=$_POST['L_B'];
$NLR_B=$_POST['NLR_B'];
$DAS_B=$_POST['DAS_B'];
$single_combination=$_POST['single_combination'];
$code_steriods=$_POST['code_steriods'];
$age=$_POST['age'];
$Gender=$_POST['Gender'];
$duration_symptoms=$_POST['duration_symptoms'];
$patient_id=$_POST['patient_id'];

$query=mysqli_query($con,"insert into rdata(RF_coded,ANA,TAL,Comorbidities,TJC_B,CRP_B,ESR_B,N_B,L_B,NLR_B,DAS_B,single_combination,code_steriods,age,Gender,duration_symptoms,patient_id) 
values('$RF_coded','$ANA','$TAL','$Comorbidities','$TJC_B','$CRP_B','$ESR_B','$N_B','$L_B','$NLR_B','$DAS_B','$single_combination','$code_steriods','$age','$Gender','$duration_symptoms','$patient_id')");
if($query)
    {echo '<script>alert("Submitted")</script>'; }
else{echo '<script>alert("Failed")</script>'; }
}
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title></title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    <title>Data Analysis</title>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;;border: 2px solid #ffffff;}
</style>
</head> 

<body>    
    <?php include('layout/dasheader.php');?>
    <div class="container-fluid check">	<br>
    
	

	
	    <div class="row ">	
		
    		<div class="col-md-3">
    			 <?php include ('layout/sidebar.php'); ?>
    		</div>
    		<div class="col-md-3">
    			<div class="card" style="width: 18rem;"> 
					  <div class="card-body">
					    <h5 class="card-title">All Data</h5>
					    <?php
			$username=$_SESSION['login'];	
			$result=mysqli_query($con,"select * from das_table");
				$number=mysqli_num_rows($result);

					echo '<div align="center" style="font-weight:bold;font-size:24px;color:gray;margin-bottom:30px;">'.$number.' <br></div>';
                
				while($rows=mysqli_fetch_array($result)){

			 ?>
					<?php } ?>    
					<a href="dasall.php" class="btn btn-primary">Click</a>
					  </div>
				</div>
    		</div>
    		<div class="col-md-3">
    			<div class="card" style="width: 18rem;"> 
					  <div class="card-body">
					    <h5 class="card-title">Results</h5>
					   <?php
			$username=$_SESSION['login'];	
			$result=mysqli_query($con,"select * from das_table where is_active= 1");
				$number=mysqli_num_rows($result);

					echo '<div align="center" style="font-weight:bold;font-size:24px;color:gray;margin-bottom:30px;">'.$number.' <br></div>';
                
				while($rows=mysqli_fetch_array($result)){

			 ?>
					<?php } ?>    
					    <a href="dasresults.php" class="btn btn-primary">Click</a>
					  </div>
				</div>
    		</div>
    		<div class="col-md-3">
    			<div class="card" style="width: 18rem;"> 
					  <div class="card-body">
					    <h5 class="card-title">Pending</h5>
					    <?php
			$username=$_SESSION['login'];	
			$result=mysqli_query($con,"select * from das_table where is_active = 0");
				$number=mysqli_num_rows($result);

					echo '<div align="center" style="font-weight:bold;font-size:24px;color:gray;margin-bottom:30px;">'.$number.' <br></div>';
                
				while($rows=mysqli_fetch_array($result)){

			 ?>
					<?php } ?>    
					    <a href="daspending.php" class="btn btn-primary">Click</a>
					  </div>
				</div>
    		</div>
    		
    </div>	
	
	
	
	
	
	
	
	
	
	
	
	
	
<br>
</div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		   
		    <div class="pt-3 text-center">
			   
		    </div>
	    </div>
    </section>

               
    
 <?php include('layout/footer.php');?>
       
    <!-- Javascript -->          
    <script src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/js/function.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

</body>
</html> 

<?php } ?>

<?php
session_start();
include('../config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:../index.php');
}
else{
if(isset($_POST['submit']))
{
$patient_id=$_POST['patient_id'];
$age=$_POST['age'];
$gender=$_POST['gender'];
$sledaiscore=$_POST['sledaiscore'];
$hb=$_POST['hb'];
$tc=$_POST['tc'];
$nl=$_POST['nl'];
$l=$_POST['l'];
$plt=$_POST['plt'];
$nlr=$_POST['nlr'];
$esr=$_POST['esr'];
$crp=$_POST['crp'];
$proc=$_POST['proc'];
$username=$_SESSION['login'];
$is_active=0;
$query=mysqli_query($con,"insert into ra_table (patient_id,age,gender,sledaiscore,hb,tc,nl,l,plt,nlr,esr,crp,procalcitonin,username,is_active)
 values('$patient_id','$age','$gender','$sledaiscore','$hb','$tc','$nl','$l','$plt','$nlr','$esr','$crp','$proc','$username','$is_active')");
if($query)
    {echo '<script>alert("Submitted")</script>'; }
else{echo '<script>alert("Failed")</script>'; }
}
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>DAS Data Insertion</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
        <title>Infection DATA</title>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
	select{
        width:100%;

        background:;
        margin-left:auto;
        margin-right:auto;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
</style>
</head> 
<body>    
    <?php include('../layout/infectionheader.php');?>
    <div class="container-fluid">   
    <div class="row">   
            <div class="col-md-2 check">
                <br>
               <?php include('../layout/infectsidebar.php'); ?>
            </div>
            <div class="col-md-5 check">
                <br>
				<div class="">
<h3 class="m-t-10 m-b-10" align="center" style="background-color: #eee;width: 100%;box-shadow: 10px;font-family: times;line-height: 50px;"><b>Rheumatoid Data</b></h3>
</div>
<form name="addpost" method="post"  style="" >
 <div class="row">
   
    <div class="col-sm-12">
        <table>
            <tr>
                <td>Patient Id</td>
                <td><input type="text"  id="patient_id" name="patient_id"  required></td>
            </tr>
            <tr>
                <td>Age</td>
                <td><input type="text"  id="age" name="age"  required></td>
            </tr>
            <tr>
                <td>Gender</td>
                <td><select  name="gender">
                        <option>Select</option>
                        <option value="Male">Male</option>
                        <option value="Female">Female</option>
                        <option value="Other">Other</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>SLE</td>
                <td><select  name="sledaiscore">
                        <option>Select</option>
                        <option value=">4.5">Active</option>
                        <option value="<4.5">Inactive</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>HB</td>
                <td><input type="text"  id="hb" name="hb"  required></td>
            </tr>
            <tr>
                <td>Total count (10^3)</td>
                <td><input type="text"  id="tc" name="tc"  required></td>
            </tr>
            <tr>
                <td>Neutrophil (%)</td>
                <td><input type="text"  id="nl" name="nl"  required></td>
            </tr>
            <tr>
                <td>Lymphocyte (%)</td>
                <td><input type="text"  id="l" name="l"  required></td>
            </tr>
            <tr>
                <td>Platelets (10^5)</td>
                <td><input type="text"  id="plt" name="plt"  required></td>
            </tr>
            <tr>
                <td>NLR</td>
                <td><input type="text"  id="nlr" name="nlr"  required></td>
            </tr>
            <tr>
                <td>ESR (mm/hr)</td>
                <td><input type="text"  id="esr" name="esr"  required></td>
            </tr>
            <tr>
                <td>CRP (mg/l)</td>
                <td><input type="text"  id="crp" name="crp"  required></td>
            </tr>
            <tr>
                <td>Procal (ng/ml)</td>
                <td><input type="text"  id="PROC" name="proc"  required></td>
            </tr>
            <tr><td></td>
                <td><button type="submit" name="submit" class="btn btn-success waves-effect waves-light">Submit</button></td>
            </tr>
        </table>
</div>
</div>   
<br>
<div align="center" class="m-t-10 m-b-20">
</div>
            </form> <br>
            </div>
            <div class="col-md-5 check">
            <table class="table dt-table" id="myTable">
                                    <thead>
                                    <tr>
                                        <th>
                                            PId
                                        </th>
                                        <th>
                                           HB
                                        </th> 
                                        <th>
                                        nl
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                        
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                          <?php 
                                           $uname=$_SESSION['login'];
                                           $query1=mysqli_query($con,"Select * from ra_table where username='$uname'");
                                                while($rows=mysqli_fetch_array($query1))
                                                {
                                               ?>                    
                                                <tr>
                                                <td><?php echo htmlentities($rows['patient_id']); ?></td>
                                                <td><?php echo htmlentities($rows['hb']); ?></td>
                                                <td><?php echo htmlentities($rows['nl']); ?></td>
                                                <td><a href="rheumatoiddetails.php?nid=<?php echo htmlentities($rows['id']);?>" class="text-dark">View</a></td>
                                                </tr>
                                        <?php } ?>
                                    </tbody>
                                    </table>
            </div>
    </div>  
    </div>
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h1 class="page-heading single-col-max mx-auto"></h1>
            <div class="page-intro single-col-max mx-auto"></div>
            <div class="main-search-box pt-3 d-block mx-auto">
             </div>
        </div>
    </div>
    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h3 class="mb-2 text-white mb-3"></h3>
            <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
            <div class="pt-3 text-center">
            </div>
        </div>
    </section>
 <?php include('../layout/footer.php');?>
 

    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        $(document).ready( function () {
    $('#myTable').DataTable();
} );
    </script>
</body>
</html> 

<?php } ?>
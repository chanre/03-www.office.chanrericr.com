<?php
session_start();
include('../config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:../index.php');
}
else{
if(isset($_POST['submit']))
{
$patient_id=$_POST['patient_id'];
$Age=$_POST['Age'];
$Gender=$_POST['Gender'];
$Duration_diagnosis=$_POST['Duration_diagnosis'];
$RF=$_POST['RF'];
$TJC=$_POST['TJC'];
$SJC=$_POST['SJC'];
$ESR=$_POST['ESR'];
$CRP=$_POST['CRP'];
$N=$_POST['N'];
$L=$_POST['L'];
$NLR=$_POST['NLR'];
$Family_history=$_POST['Family_history'];
$Smoking_history=$_POST['Smoking_history'];
$username=$_SESSION['login'];
$is_active=0;
$query=mysqli_query($con,"insert into das_table (patient_id,Age,Gender,Duration_diagnosis,RF,TJC,SJC,ESR,CRP,N,L,NLR,Family_history,Smoking_history,username,is_active) values('$patient_id','$Age','$Gender','$Duration_diagnosis','$RF','$TJC','$SJC','$ESR','$CRP','$N','$L','$NLR','$Family_history','$Smoking_history','$username','$is_active')");


if($query)
    {echo '<script>alert("Submitted")</script>'; }
else{echo '<script>alert("Failed")</script>'; }
}
?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title></title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
        text-align:center;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
</style>
</head> 

<body>    
    <?php include('../layout/infectionheader.php');?>
    <div class="container-fluid">   
    <div class="row">   
            <div class="col-md-2 check">
                <br>
               <?php include('../layout/sidebar.php'); ?>
            </div>
            <div class="col-md-6 check">
                <br>
            <?php $id=intval($_GET['nid']);
            $query1= mysqli_query($con,"select * from infection_table where id = '$id'");
            while($rows=mysqli_fetch_array($query1)){
            
            ?>
            <form name="addpost" method="post" enctype="multipart/form-data">
            <div class="">
            <h3 class="m-t-10 m-b-10" align="center" style="background-color: #eee;width: 100%;box-shadow: 10px;font-family: times;line-height: 50px;"><b>Infection Data</b></h3>
           </div>
            <table>
                <tr>
                    <td></td>
                    <td><input type="text" class="" id="username" name="username" value="<?php echo $_SESSION['login']; ?>"  required hidden></td>
                </tr>
                 <tr>
                    <td>Patient Id</td>
                    <td><input type="text" class="form-control" id="patient_id" name="patient_id" value="<?php echo htmlentities($rows['patient_id']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>Age</td>
                    <td><input type="text" class="form-control" id="patient_id" name="patient_id" value="<?php echo htmlentities($rows['age']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td><input type="text" class="form-control" id="patient_id" name="gender" value="<?php echo htmlentities($rows['gender']); ?>"readonly required></td>
                </tr>
               
                <tr>
                    <td>HB</td>
                    <td><input type="text" class="" id="hb" name="hb"  readonly required value="<?php echo htmlentities($rows['hb']); ?>"> </td>
                </tr>
                <tr>
                    <td>Total count (10^3)</td>
                    <td><input type="text" class="" id="tc" name="tc" value="<?php echo htmlentities($rows['tc']); ?>" required></td>
                </tr>
                <tr>
                    <td>Neutrophil (%)</td>
                    <td><input type="text" id="nl" name="nl" value="<?php echo htmlentities($rows['nl']); ?>" required></td>
                </tr>
               
                <tr>
                    <td>Lymphocyte (%)</td>
                    <td><input type="text" class="" id="l" name="l" value="<?php echo htmlentities($rows['l']); ?>" required></td>
                </tr>
             
                <tr>
                    <td>Platelets (10^5)</td>
                    <td><input type="text" class="" id="plt" name="plt" value="<?php echo htmlentities($rows['plt']); ?>" required></td>
                </tr>
                <tr>
                    <td>nlr</td>
                    <td><input type="text" class="" id="nlr" name="nlr" value="<?php echo htmlentities($rows['nlr']); ?>" required></td>
                </tr>
                <tr>
                    <td>ESR (mm/hr)</td>
                    <td><input type="text" class="" id="esr" name="esr" value="<?php echo htmlentities($rows['esr']); ?>" required></td>
                </tr>
                <tr>
                    <td>CRP (mg/l)</td>
                    <td><input type="text" class="" id="crp" name="crp" value="<?php echo htmlentities($rows['crp']); ?>" required></td>
                </tr>
                <tr>
                    <td>Procal (ng/ml)</td>
                    <td><input type="text" class="" id="PROCALCITONIN" name="proc" value="<?php echo htmlentities($rows['procalcitonin']); ?>" required></td>
                </tr>
          
                
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table
<br>
<div align="center">
<h2 style="color:white;">Result = <?php echo htmlentities($rows['results']); ?></h2>

</div><br>

            </form> <?php } ?><br>
            </div>
            <div class="col-md-4 check">
            
            </div>

    </div>  
    </div>  
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h1 class="page-heading single-col-max mx-auto"></h1>
            <div class="page-intro single-col-max mx-auto"></div>
            <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
        </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h3 class="mb-2 text-white mb-3"></h3>
            <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
            <div class="pt-3 text-center">
               
            </div>
        </div>
    </section>

               
    
 <?php include('layout/footer.php');?>

    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
</body>
</html> 

<?php } ?>
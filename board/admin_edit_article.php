
<?php
session_start();
include('../config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$posttitle=$_POST['posttitle'];
$postdetails=$_POST['postdescription'];


$username= $_SESSION['login'];
$postid=intval($_GET['pid']);
$query=mysqli_query($con,"update tblposts set PostTitle='$posttitle',PostDetails='$postdetails',updateduser='$username' where id='$postid'");
if($query)
{
$msg="Post successfully added ";
}
else{
$error="Something went wrong...Please try again.";    
} 

}
?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Edit Article</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
	select{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
	.myarticle{background:white;color:black;}
    form{margin-left:auto;margin-right:auto; width:70%;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
</style>
</head> 

<body>    
    <?php include('../layout/articleheader.php');?>
    <div class="container-fluid">  
<div class="row">
<div class="col-sm-6">  
<!---Success Message--->  
<?php if($msg){ ?>
<div class="alert alert-success" role="alert">
<strong>Well done!</strong> <?php echo htmlentities($msg);?>
</div>
<?php } ?>

<!---Error Message--->
<?php if($error){ ?>
<div class="alert alert-danger" role="alert">
<strong>Oh snap!</strong> <?php echo htmlentities($error);?></div>
<?php } ?>


</div>
</div>	
    <div class="row">   
            <div class="col-md-2 check">
                <br>
               <?php include('../admin/layout/sidebar.php'); ?>
            </div>
            <div class="col-md-10 check">
			<br>
			<?php
$postid=intval($_GET['pid']);
$myquery=mysqli_query($con,"Select * from tblposts where id='$postid'");
while($rows=mysqli_fetch_array($myquery))
{
?>
              <form name="addpost" method="post" enctype="multipart/form-data">
				<div class="form-group m-b-20">
				<label for="exampleInputEmail1">Post Title</label>
				<input type="text" class="form-control" id="posttitle" name="posttitle" placeholder="Enter title" value="<?php echo htmlentities($rows['PostTitle']);?>" required>
				</div>
				 <div>
				<label for="exampleInputEmail1">Post Details</label>
				<div class="myarticle" >
				<textarea class="summernote" name="postdescription" required><?php echo htmlentities($rows['PostDetails']);?></textarea>
				</div>
				</div>
				<br>
				<br>
				<button type="submit" name="submit" class="btn btn-warning waves-effect waves-light">Update Post</button>
				
				 <br>
				</form>
														
				<?php } ?>
				<br>
        </div>
    </div>  
    </div>  
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h1 class="page-heading single-col-max mx-auto"></h1>
            <div class="page-intro single-col-max mx-auto"></div>
            <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
        </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h3 class="mb-2 text-white mb-3"></h3>
            <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
            <div class="pt-3 text-center">
               
            </div>
        </div>
    </section>

               
    
 <?php include('../layout/footer.php');?>

    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
 <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
<script>
            jQuery(document).ready(function(){

                $('.summernote').summernote({
                    height: 340,                 // set editor height
                    minHeight: null,             // set minimum height of editor
                    maxHeight: null,             // set maximum height of editor
                    focus: false                 // set focus to editable area after initializing summernote
                });
                // Select2
                $(".select2").select2();

                $(".select2-limiting").select2({
                    maximumSelectionLength: 2
                });
            });
        </script>

</body>
</html> 

<?php } ?>

<?php
session_start();
include('../config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$posttitle=$_POST['posttitle'];
$postdetails=$_POST['postdescription'];
$arr = explode(" ",$posttitle);
$url=implode("-",$arr);
$imgfile=$_FILES["postimage"]["name"];
$extension = substr($imgfile,strlen($imgfile)-4,strlen($imgfile));
$allowed_extensions = array(".jpg","jpeg",".png",".gif");
if(!in_array($extension,$allowed_extensions))
{
echo "<script>alert('Invalid format. Only jpg / jpeg/ png /gif format allowed');</script>";
}
else
{
$imgnewfile=md5($imgfile).$extension;
move_uploaded_file($_FILES["postimage"]["tmp_name"],"postimages/".$imgnewfile);

$status=1;
$query=mysqli_query($con,"insert into tblposts(PostTitle,PostDetails,PostUrl,Is_Active,PostImage) values('$posttitle','$postdetails','$url','$status','$imgnewfile')");
if($query)
{
$msg="Post successfully added ";
}
else{
$error="Something went wrong... Please try again.";    
} 
}
}
?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Article and Posts Insertion</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
	select{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
	.myarticle{background:white;color:black;}
    form{}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
	.card{
		margin-bottom:10px;
		width:100%;
		
	}
	th{
		text-align:center;
	}
	table{margin-left:auto;margin-right:auto;margin-bottom: 35px;color:black;
border-radius: 15px;
background: white;
box-shadow:  10px 10px 20px #10492b, 
             -10px -10px 20px #40ffab;}
	tr{border:2px solid gray;}
</style>
</head> 

<body>    
    <?php include('../layout/articleheader.php');?>
    <div class="container-fluid">   
	
    <div class="row">   
          
			
            <div class="col-md-10 check">
			<br>
		<br>
      <div class="row">
    <table bolder="3">
		<tr>
		<th>Tiltle</th>
		<th>Writer</th>
		<th>Action</th>
		
		</tr>
			<?php $query=mysqli_query($con,"Select * from tblposts");
			while($rows= mysqli_fetch_array($query))
			{
			?>
		<tr>
		<td><?php echo htmlentities($rows['PostTitle']);?></td>
		<td><?php echo htmlentities($rows['username']);?></td>
		<td><a href="details.php?nid=<?php echo htmlentities($rows['id'])?> <?php echo htmlentities($rows['PostUrl'])?>" class="btn btn-warning">Read</a> |<a href="edit.php?pid=<?php echo htmlentities($rows['id'])?>" class="btn btn-danger">Edit</a></td>
		</tr>
		<?php } ?>
		</table>
		
		
		
        <div class="col-md-4"> 
	
		
			
        </div>
		
   
      </div>
			
			</div>
             <div class="col-md-2 check">
                <br>
               <?php include('../layout/articlesidebar.php'); ?>
            </div>

    </div>  
    </div>  
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h1 class="page-heading single-col-max mx-auto"></h1>
            <div class="page-intro single-col-max mx-auto"></div>
            <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
        </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
    
    </section>

               
    
 <?php include('../layout/footer.php');?>

    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  


</body>
</html> 

<?php } ?>
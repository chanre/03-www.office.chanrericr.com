<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$text =$_POST['text'];
$user= $_SESSION['login'];

$query=mysqli_query($con,"insert into message (username,text) values('$user','$text')");
}
?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title></title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    <title>Data Analysis</title>
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    form{background:#a8b3ad;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
    .chat{
        width:90%;
       height:450px;
        background:white;
        color: black;
        margin-left: auto;
        margin-right: auto;
        overflow: scroll;
    }
    .typing{
        width:80%;
       
      
        margin-left: auto;
        margin-right: auto;
    }
   .form-group{
    
    background-color: skyblue;
   overflow: hidden; 
   border-radius: 10px;
   margin-bottom: 2px;
   /*padding: -15px;*/
    }
  
.name{
    width:30%;
    float:left;
    font-size: 14px;
   font-family: times;
  font-weight: bold;
  padding-left: 20px;
    }
  .text{
    width:95%;
    float:left;
    margin-left:5px;
    word-wrap: break-word; 
    background: #4080ff;
    padding: 3%; 
    color: white; 
    border-radius: 10px;
    }
</style>
</head> 

<body>    
   <header class="header fixed-top">	    
        <div class="branding docs-branding">
            <div class="container-fluid position-relative py-2">
                <div class="docs-logo-wrapper">
	                <div class="site-logo"><a class="navbar-brand" href="dashboard.php"><img class="logo-icon mr-2" src="assets/images/coderdocs-logo.svg" alt="logo"><span class="logo-text">Message <span class="text-alt">Box</span></span></a></div>    
                </div>
	            <div class="docs-top-utilities d-flex justify-content-end align-items-center">
	
					<ul class="social-list list-inline mx-md-3 mx-lg-5  d-none d-lg-flex">
						<li class="list-inline-item"><div class="dropdown">
  <button class="bg-outline-primary dropdown-toggle" type="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
   <?php echo $_SESSION['login']; ?>
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="logout.php">Sign Out</a>
    <a class="dropdown-item" href="changepassword.php">Change Password</a>
   
  </div>
</div></li>
<li class="list-inline-item"><small><a href=""></a></small></li>
		            </ul>
		           
	            </div>
            </div>
        </div>
    </header>
    <div class="container-fluid">	
    <div class="row">	
    		<div class="col-md-2 check">
                <br>
               <?php include('layout/sidebar.php'); ?>
            </div>
    		<div class="col-md-6 check">
                <br>
            <form name="addpost" method="post" enctype="multipart/form-data">
            <div class="">
            <h3 class="m-t-10 m-b-10" align="center" style="background-color: #eee;width: 100%;box-shadow: 10px;font-family: times;line-height: 50px;"><b>Messages</b></h3>
           </div>
            <div class="chat" >
                <div class="msg" id="div1">
                    <?php $query2=mysqli_query($con,"Select * from message");
                    while ($rows=mysqli_fetch_array($query2)) {
                        
                     ?>
                    <div class="form-group">
                        <label class="name"><?php echo htmlentities($rows['username']); ?> </label>
                        <label  class="name">(<?php echo htmlentities($rows['time']); ?> )</label>
                        <p class="text"><?php echo htmlentities($rows['text']); ?> </p>

                    </div>
                <?php } ?>
                </div>
            </div>  
<br>
<div class="typing" align="right">
<textarea class="form-control" placeholder="Type your message.." name="text"></textarea>

<button type="submit" name="submit"  class="btn btn-success waves-effect waves-light">Send</button> &nbsp;
</div><br>
</form> <br>
<script type="text/javascript">
   $("#div1").animate({ scrollTop: $('#div1').prop("scrollHeight")}, 1000);
</script>
    		</div>
    		<div class="col-md-4 check">
            
            </div>
    </div>	
    </div>	
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h1 class="page-heading single-col-max mx-auto"></h1>
		    <div class="page-intro single-col-max mx-auto"></div>
		    <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
	    </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h3 class="mb-2 text-white mb-3"></h3>
		    <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
		    <div class="pt-3 text-center">
			   
		    </div>
	    </div>
    </section>

               
    
 <?php include('layout/footer.php');?>

    <!-- Javascript -->          
    <script src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/js/function.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

</body>
</html> 

<?php } ?>
<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$RF_coded=$_POST['RF_coded'];
$ANA=$_POST['ANA'];
$TAL=$_POST['TAL'];
$Comorbidities=$_POST['Comorbidities'];
$TJC_B=$_POST['TJC_B'];
$CRP_B=$_POST['CRP_B'];
$ESR_B=$_POST['ESR_B'];
$N_B=$_POST['N_B'];
$L_B=$_POST['L_B'];
$NLR_B=$_POST['NLR_B'];
$DAS_B=$_POST['DAS_B'];
$single_combination=$_POST['single_combination'];
$code_steriods=$_POST['code_steriods'];
$age=$_POST['age'];
$Gender=$_POST['Gender'];
$duration_symptoms=$_POST['duration_symptoms'];
$patient_id=$_POST['patient_id'];
$username=$_SESSION['login'];
$id=intval($_GET['nid']);
$query=mysqli_query($con,"update rdata SET RF_coded='$RF_coded',ANA='$ANA',TAL='$TAL',Comorbidities='$Comorbidities',TJC_B='$TJC_B',CRP_B='$CRP_B',ESR_B='$ESR_B',N_B='$N_B',L_B='$L_B',NLR_B='$NLR_B',DAS_B='$DAS_B',single_combination='$single_combination',code_steriods='$code_steriods',age='$age',Gender='$Gender',duration_symptoms='$duration_symptoms',patient_id='$patient_id',username='$username',is_active='0' where id='$id'");if($query)
    {echo '<script>alert("Submitted")</script>'; }
else{echo '<script>alert("Failed")</script>'; }
}
?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title></title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
		text-align:center;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
</style>
</head> 

<body>    
    <?php include('layout/header.php');?>
    <div class="container-fluid">	
    <div class="row">	
    		<div class="col-md-2 check">
                <br>
               <?php include('layout/sidebar.php'); ?>
            </div>
    		<div class="col-md-6 check">
                <br>
			<?php $id=intval($_GET['nid']);
            $query1= mysqli_query($con,"select * from rdata where id='$id'");
			while($rows=mysqli_fetch_array($query1)){
			
			?>
            <form name="addpost" method="post" enctype="multipart/form-data">
            <div class="">
            <h3 class="m-t-10 m-b-10" align="center" style="background-color: #eee;width: 100%;box-shadow: 10px;font-family: times;line-height: 50px;"><b>INSERTION FORM</b></h3>
           </div>
            <table>
                <tr>
                    <td></td>
                    <td><input type="text" class="" id="username" name="username" value="<?php echo $_SESSION['login']; ?>"  required hidden></td>
                </tr>
                 <tr>
                    <td>Patient Id</td>
                    <td><input type="text" class="form-control" id="patient_id" name="patient_id" value="<?php echo htmlentities($rows['patient_id']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>RF_coded</td>
                    <td><input type="text" class="form-control" id="RF_coded" name="RF_coded" value="<?php echo htmlentities($rows['RF_coded']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>ANA</td>
                    <td><input type="text" class="form-control" id="ANA" name="ANA" value="<?php echo htmlentities($rows['ANA']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>Time to Achive LDA < 2.6 in months</td>
                    <td><input type="text" class="form-control" id="TAL" name="TAL" value="<?php echo htmlentities($rows['TAL']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>Comorbidities</td>
                    <td><input type="text" class="form-control" id="Comorbidities" name="Comorbidities" value="<?php echo htmlentities($rows['Comorbidities']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>TJC_B</td>
                    <td><input type="text" class="form-control" id="TJC_B" name="TJC_B" value="<?php echo htmlentities($rows['TJC_B']); ?>"readonly required></td>
                </tr>
             
                <tr>
                    <td>CRP_B</td>
                    <td><input type="text" class="form-control" id="CRP_B" name="CRP_B" value="<?php echo htmlentities($rows['CRP_B']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>ESR_B</td>
                    <td><input type="text" class="form-control" id="ESR_B" name="ESR_B" value="<?php echo htmlentities($rows['ESR_B']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>N_B</td>
                    <td><input type="text" class="form-control" id="N_B" name="N_B" value="<?php echo htmlentities($rows['N_B']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>L_B</td>
                    <td><input type="text" class="form-control" id="L_B" name="L_B" value="<?php echo htmlentities($rows['L_B']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>NLR_B</td>
                    <td><input type="text" class="form-control" id="NLR_B" name="NLR_B" value="<?php echo htmlentities($rows['NLR_B']); ?>"readonly required></td>
                </tr>
                <tr>
                    <td>DAS_B</td>
                    <td><input type="text" class="form-control" id="DAS_B" name="DAS_B" value="<?php echo htmlentities($rows['DAS_B']); ?>" readonly required></td>
                </tr>
              
                <tr>
                    <td>single_combination</td>
                    <td><input type="text" class="form-control" id="single_combination" name="single_combination" value="<?php echo htmlentities($rows['single_combination']); ?>" readonly required></td>
                </tr>
                <tr>
                    <td>code_steriods</td>
                    <td><input type="text" class="form-control" id="code_steriods" name="code_steriods" value="<?php echo htmlentities($rows['code_steriods']); ?>" readonly required></td>
                </tr>
                <tr>
                    <td>age</td>
                    <td><input type="text" class="form-control" id="age" name="age" value="<?php echo htmlentities($rows['age']); ?>" readonly required></td>
                </tr>
                <tr>
                    <td>Gender</td>
                    <td><input type="text" class="form-control" id="Gender" name="Gender" value="<?php echo htmlentities($rows['Gender']); ?>" readonly required></td>
                </tr>
                <tr>
                    <td>duration_symptoms</td>
                    <td><input type="text" class="form-control" id="duration_symptoms" name="duration_symptoms" value="<?php echo htmlentities($rows['duration_symptoms']); ?>" readonly required></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
            </table
<br>
<div align="center">
<h2 style="color:white;">Result = <?php echo htmlentities($rows['results']); ?></h2>

</div><br>

			</form> <?php } ?><br>
    		</div>
    		<div class="col-md-4 check">
            
            </div>

    </div>	
    </div>	
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h1 class="page-heading single-col-max mx-auto"></h1>
		    <div class="page-intro single-col-max mx-auto"></div>
		    <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
	    </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h3 class="mb-2 text-white mb-3"></h3>
		    <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
		    <div class="pt-3 text-center">
			   
		    </div>
	    </div>
    </section>

               
    
 <?php include('layout/footer.php');?>

    <!-- Javascript -->          
    <script src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/js/function.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
<script>
            $(document).on("click", "#save", function () {
                //get value of message 
                var patient_id = $("#patient_id").val();
                var RF_coded = $("#RF_coded").val();
                var ANA = $("#ANA").val();
                var TAL = $("#TAL").val();
                var Comorbidities = $("#Comorbidities").val();
                var TJC_B = $("#TJC_B").val();
                var CRP_B = $("#CRP_B").val();
                var ESR_B = $("#ESR_B").val();
                var N_B = $("#N_B").val();
                var L_B = $("#L_B").val();
                var NLR_B = $("#NLR_B").val();
                var DAS_B = $("#DAS_B").val();
                var code_steriods = $("#code_steriods").val();
                var age = $("#age").val();
                var Gender = $("#Gender").val();
                var duration_symptoms = $("#duration_symptoms").val();
               

                //check if value is not empty
              
                //Ajax call to send data to the insert.php
                $.ajax({
                    type: "POST",
                    url: "insert.php",
                    data: {
                        patient_id : patient_id,
                        RF_coded:RF_coded,
                        ANA:ANA,TAL:TAL,Comorbidities:Comorbidities,TJC_B:TJC_B,CRP_B:CRP_B,ESR_B:ESR_B,N_B:N_B, 
                        L_B:L_B,NLR_B:NLR_B,DAS_B:DAS_B,code_steriods:code_steriods,age:age,Gender:Gender,duration_symptoms:duration_symptoms

                        

                    },
                    cache: false,
                    success: function (data) {
                        //Insert data before the message wrap div
                        $(data).insertBefore(".message-wrap:first");
                        //Clear the textarea message
                        $("#message").val("");
                    }
                });
            });
        </script>
</body>
</html> 

<?php } ?>
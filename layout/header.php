<header class="header fixed-top">
    <div class="branding docs-branding">
        <div class="container-fluid position-relative py-2">
            <div class="docs-logo-wrapper">
                <div class="site-logo"><a class="navbar-brand" href="remission_insert.php"><img class="logo-icon mr-2"
                            src="assets/images/coderdocs-logo.svg" alt="logo"><span class="logo-text">Remission<span
                                class="text-alt">Data</span></span></a></div>
            </div>
            <div class="docs-top-utilities d-flex justify-content-end align-items-center">

                <ul class="social-list list-inline mx-md-3 mx-lg-5  d-none d-lg-flex">
                    <li class="list-inline-item">
                        <div class="dropdown">
                            <button class="bg-outline-primary dropdown-toggle" type="" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?php echo $_SESSION['login']; ?>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="logout.php">Sign Out</a>
                                <a class="dropdown-item" href="changepassword.php">Change Password</a>

                            </div>
                        </div>
                    </li>
                    <li class="list-inline-item"><small><a href=""></a></small></li>
                </ul>

            </div>
        </div>
    </div>
</header>
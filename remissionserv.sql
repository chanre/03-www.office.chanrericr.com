-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 05:41 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `remissionserv`
--

-- --------------------------------------------------------

--
-- Table structure for table `admintable`
--

CREATE TABLE `admintable` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admintable`
--

INSERT INTO `admintable` (`id`, `name`, `email`, `institute`, `password`, `creationDate`) VALUES
(1, 'admin', 'webdesigner@chanrejournals.com', 'CRICR', '$2y$12$qq9GwrLuWmJ/W7VZ7CGsheDcawfLAyWrCweRAeaIswCdb8aMBIV4K', '2020-09-11 06:08:25');

-- --------------------------------------------------------

--
-- Table structure for table `doctortable`
--

CREATE TABLE `doctortable` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `institute` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctortable`
--

INSERT INTO `doctortable` (`id`, `name`, `email`, `institute`, `password`, `creationDate`) VALUES
(3, 'shamim', 'abc@gmail.com', 'chanre', '$2y$12$i2AwqvlkVXNo/cLIJRdXaO44VZgs4Qw/ntrbaJBmdKA9oh154zMJW', '2020-09-09 05:55:54');

-- --------------------------------------------------------

--
-- Table structure for table `rdata`
--

CREATE TABLE `rdata` (
  `id` int(30) NOT NULL,
  `RF_coded` int(30) NOT NULL,
  `ANA` int(30) NOT NULL,
  `TAL` int(30) NOT NULL,
  `Comorbidities` int(30) NOT NULL,
  `TJC_B` int(30) NOT NULL,
  `CRP_B` int(30) NOT NULL,
  `ESR_B` int(30) NOT NULL,
  `N_B` int(30) NOT NULL,
  `L_B` int(30) NOT NULL,
  `NLR_B` int(30) NOT NULL,
  `DAS_B` int(30) NOT NULL,
  `single_combination` int(30) NOT NULL,
  `code_steriods` int(30) NOT NULL,
  `age` int(30) NOT NULL,
  `Gender` int(30) NOT NULL,
  `duration_symptoms` int(30) NOT NULL,
  `patient_id` varchar(10) NOT NULL,
  `results` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `username` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rdata`
--

INSERT INTO `rdata` (`id`, `RF_coded`, `ANA`, `TAL`, `Comorbidities`, `TJC_B`, `CRP_B`, `ESR_B`, `N_B`, `L_B`, `NLR_B`, `DAS_B`, `single_combination`, `code_steriods`, `age`, `Gender`, `duration_symptoms`, `patient_id`, `results`, `is_active`, `username`) VALUES
(1, 1, 2, 2, 1, 1, 2, 2, 2, 2, 1, 1, 2, 2, 2, 2, 3, '1234', 1, 1, 'shamim'),
(2, 1, 2, 3, 4, 5, 1, 1, 2, 3, 3, 3, 3, 3, 2, 2, 2, '1214', 1, 1, 'shamim'),
(3, 2, 2, 2, 3, 2, 1, 2, 3, 1, 2, 2, 1, 2, 3, 3, 3, '3456', 1, 1, 'shamim'),
(4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, '2', 1, 1, 'shamim'),
(5, 3, 4, 5, 4, 7, 8, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, '2', 1, 1, 'shamim');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admintable`
--
ALTER TABLE `admintable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctortable`
--
ALTER TABLE `doctortable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rdata`
--
ALTER TABLE `rdata`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admintable`
--
ALTER TABLE `admintable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `doctortable`
--
ALTER TABLE `doctortable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `rdata`
--
ALTER TABLE `rdata`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

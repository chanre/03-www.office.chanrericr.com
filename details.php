<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$RF_coded=$_POST['RF_coded'];
$ANA=$_POST['ANA'];
$TAL=$_POST['TAL'];
$Comorbidities=$_POST['Comorbidities'];
$TJC_B=$_POST['TJC_B'];
$CRP_B=$_POST['CRP_B'];
$ESR_B=$_POST['ESR_B'];
$N_B=$_POST['N_B'];
$L_B=$_POST['L_B'];
$NLR_B=$_POST['NLR_B'];
$DAS_B=$_POST['DAS_B'];
$single_combination=$_POST['single_combination'];
$code_steriods=$_POST['code_steriods'];
$age=$_POST['age'];
$Gender=$_POST['Gender'];
$duration_symptoms=$_POST['duration_symptoms'];
$patient_id=$_POST['patient_id'];
$username=$_SESSION['login'];

$id=intval($_GET['nid']);
$query=mysqli_query($con,"update rdata SET RF_coded='$RF_coded',ANA='$ANA',TAL='$TAL',Comorbidities='$Comorbidities',TJC_B='$TJC_B',CRP_B='$CRP_B',ESR_B='$ESR_B',N_B='$N_B',L_B='$L_B',NLR_B='$NLR_B',DAS_B='$DAS_B',single_combination='$single_combination',code_steriods='$code_steriods',age='$age',Gender='$Gender',duration_symptoms='$duration_symptoms',patient_id='$patient_id',username='$username',is_active='0' where id='$id'");if($query)
    {echo '<script>alert("Submitted")</script>'; }
else{echo '<script>alert("Failed")</script>'; }
}
?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title></title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="assets/css/theme.css">
    <style>
    select,
    input[type=text] {
        width: 100%;
        background: ;
        margin-left: auto;
        margin-right: auto;
    }

    form {
        background: #152b1f;
        color: white;
    }

    tr td {
        padding: 10px;
    }

    .check {
        background: #28b76b;
        color: white;
        border: 2px solid #ffffff;
    }
</style>
</head> 

<body>    
    <?php include('layout/header.php');?>
    <div class="container-fluid">	
    <div class="row">	
    		<div class="col-md-2 check">
                <br>
               <?php include('layout/sidebar.php'); ?>
            </div>
    		<div class="col-md-6 check">
                <br>
			<?php $id=intval($_GET['nid']);
            $query1= mysqli_query($con,"select * from rdata where id='$id'");
			while($rows=mysqli_fetch_array($query1)){
			
			?>
            <form name="addpost" method="post" enctype="multipart/form-data">
            <div class="">
            <h3 class="m-t-10 m-b-10" align="center" style="background-color: #eee;width: 100%;box-shadow: 10px;font-family: times;line-height: 50px;"><b>Result</b></h3>
           </div>
            <table>
            <tr>
                            <td>Remission Achieved</td>
                            <td>
                            <select id="remission" name="remission">
									  <option value="3" <?php if($rows['remission'] == '3') { ?> selected="selected"<?php } ?>>3 months</option>
							          <option value="12" <?php if($rows['remission'] == '12') { ?> selected="selected"<?php } ?>>12 months</option>
							</select>


                                <!-- <select name="remission" id="remission">
                                    <option value="3">3 Months</option>
                                    <option value="12">12 Months</option>
                                </select> -->
                            </td>
                        </tr>
                        <tr>
                            <td>Age(In Years)</td>
                            <td><input type="text" value="<?php echo htmlentities($rows['age']); ?>" class="" id="age" name="age" required></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>
                            <select name="Gender" id="Gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select> 
                            <!-- <input type="text" class="" id="Gender" name="Gender" required></td> -->
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="text" class="" id="username" name="username"
                                    value="<?php echo $_SESSION['login']; ?>" required hidden></td>
                        </tr>
                        <tr>
                            <td>Patient Id</td>
                            <td><input type="text" value="<?php echo htmlentities($rows['patient_id']); ?>" class="" id="patient_id" name="patient_id" required></td>
                        </tr>
                        <tr>
                            <td>Duration of Illness (in months)</td>
                            <td><input type="text" class="" value="<?php echo htmlentities($rows['duration']); ?>" id="duration" name="duration" required></td>
                        </tr>
                        <tr>
                            <td>Hypertension</td>
                            <td>
                            <select id="hypertension" name="hypertension">
									  <option value="1" <?php if($rows['hypertension'] == '1') { ?> selected="selected"<?php } ?>>Present</option>
							          <option value="0" <?php if($rows['hypertension'] == '0') { ?> selected="selected"<?php } ?>>Not Present</option>
							</select>
                                <!-- <select name="hypertension" id="hypertension">
                                    <option value="1">Present</option>
                                    <option value="0">Not Present</option>
                                </select> -->
                            </td>
                        </tr>

                        <tr>
                            <td>Diabetes</td>
                            <td>
                                <!-- <select name="diabetes" id="diabetes">
                                    <option value="1">Present</option>
                                    <option value="0">Not Present</option>
                                </select> -->
                                <select id="diabetes" name="diabetes">
									  <option value="1" <?php if($rows['diabetes'] == '1') { ?> selected="selected"<?php } ?>>Present</option>
							          <option value="0" <?php if($rows['diabetes'] == '0') { ?> selected="selected"<?php } ?>>Not Present</option>
							</select>
                            </td>
                        </tr>
                        <tr>
                            <td>Thyroid</td>
                            <td>
                               
                                <select id="thyroid" name="thyroid">
									  <option value="1" <?php if($rows['thyroid'] == '1') { ?> selected="selected"<?php } ?>>Present</option>
							          <option value="0" <?php if($rows['thyroid'] == '0') { ?> selected="selected"<?php } ?>>Not Present</option>
							</select>
                            </td>
                        </tr>
                        <tr>
                            <td>TJC 28</td>
                            <td><input type="text" class="" value="<?php echo htmlentities($rows['tjc28']); ?>" id="tjc28" name="tjc28" required></td>
                        </tr>
                        <tr>
                            <td>SJC 28</td>
                            <td><input type="text" class="" id="sjc28" value="<?php echo htmlentities($rows['sjc28']); ?>" name="sjc28" required></td>
                        </tr>
                        <tr>
                            <td>TJC 68</td>
                            <td><input type="text" class=""value="<?php echo htmlentities($rows['tjc68']); ?>" id="tjc68" name="tjc68" required></td>
                        </tr>
                        <tr>
                            <td>SJC 68</td>
                            <td><input type="text" class="" id="sjc68" value="<?php echo htmlentities($rows['sjc68']); ?>" name="sjc68" required></td>
                        </tr>
                        <tr>
                            <td>Rheumatoid Factor</td>
                            <td>
                               
                                <select id="rf" name="rf">
									  <option value="1" <?php if($rows['rf'] == '1') { ?> selected="selected"<?php } ?>>Present</option>
							          <option value="0" <?php if($rows['rf'] == '0') { ?> selected="selected"<?php } ?>>Not Present</option>
							</select>
                            </td>
                        </tr>
                        <tr>
                            <td>ANA</td>
                            <td>
                                

                                <select id="ana" name="ana">
									  <option value="1" <?php if($rows['ana'] == '1') { ?> selected="selected"<?php } ?>>Present</option>
							          <option value="0" <?php if($rows['ana'] == '0') { ?> selected="selected"<?php } ?>>Not Present</option>
							</select>
                            </td>
                        </tr>

                        <tr>
                            <td>ESR(mm/hr)</td>
                            <td><input type="text" class="" value="<?php echo htmlentities($rows['esr']); ?>" id="esr" name="esr" required></td>
                        </tr>
                        <tr>
                            <td>CRP(mg/L)</td>
                            <td><input type="text" class="" id="crp" value="<?php echo htmlentities($rows['crp']); ?>" name="crp" required></td>
                        </tr>
                        <tr>
                            <td>Neutrophil (%)</td>
                            <td><input type="text" class=""value="<?php echo htmlentities($rows['neutrophil']); ?>" id="Neutrophil" name="Neutrophil" required></td>
                        </tr>
                        <tr>
                            <td>Lymphocytes (%)</td>
                            <td><input type="text" class="" value="<?php echo htmlentities($rows['lymphocytes']); ?>"  id="Lymphocytes" name="Lymphocytes" required onkeyup="nlr()"></td>
                        </tr>
                        <tr>
                            <td>NLR</td>
                            <td><input type="text" class="" value="<?php echo htmlentities($rows['nlr']); ?>" id="NLR" name="NLR" required></td>
                        </tr>
                        <tr>
                            <td>DAS 28 at Baseline</td>
                            <td><input type="text" class="" value="<?php echo htmlentities($rows['das_baseline']); ?>" id="DASB" name="DASB" required></td>
                        </tr>
                        <tr>
                            <td>Result</td>
                            <td><input type="text" class="" value="<?php echo htmlentities($rows['result']); ?>" id="DASB" name="DASB" required></td>
                        </tr>
            </table
<br>
<div align="center">

</div><br>

			</form> <?php } ?><br>
    		</div>
    		<div class="col-md-4 check">
           
            </div>

    </div>	
    </div>	
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h1 class="page-heading single-col-max mx-auto"></h1>
		    <div class="page-intro single-col-max mx-auto"></div>
		    <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
	    </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h3 class="mb-2 text-white mb-3"></h3>
		    <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
		    <div class="pt-3 text-center">
			   
		    </div>
	    </div>
    </section>

               
    
 <?php include('layout/footer.php');?>

    <!-- Javascript -->          
    <script src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/js/function.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
<script>
            $(document).on("click", "#save", function () {
                //get value of message 
                var patient_id = $("#patient_id").val();
                var RF_coded = $("#RF_coded").val();
                var ANA = $("#ANA").val();
                var TAL = $("#TAL").val();
                var Comorbidities = $("#Comorbidities").val();
                var TJC_B = $("#TJC_B").val();
                var CRP_B = $("#CRP_B").val();
                var ESR_B = $("#ESR_B").val();
                var N_B = $("#N_B").val();
                var L_B = $("#L_B").val();
                var NLR_B = $("#NLR_B").val();
                var DAS_B = $("#DAS_B").val();
                var code_steriods = $("#code_steriods").val();
                var age = $("#age").val();
                var Gender = $("#Gender").val();
                var duration_symptoms = $("#duration_symptoms").val();
               

                //check if value is not empty
              
                //Ajax call to send data to the insert.php
                $.ajax({
                    type: "POST",
                    url: "insert.php",
                    data: {
                        patient_id : patient_id,
                        RF_coded:RF_coded,
                        ANA:ANA,TAL:TAL,Comorbidities:Comorbidities,TJC_B:TJC_B,CRP_B:CRP_B,ESR_B:ESR_B,N_B:N_B, 
                        L_B:L_B,NLR_B:NLR_B,DAS_B:DAS_B,code_steriods:code_steriods,age:age,Gender:Gender,duration_symptoms:duration_symptoms

                        

                    },
                    cache: false,
                    success: function (data) {
                        //Insert data before the message wrap div
                        $(data).insertBefore(".message-wrap:first");
                        //Clear the textarea message
                        $("#message").val("");
                    }
                });
            });
        </script>
</body>
</html> 

<?php } ?>
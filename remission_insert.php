<?php
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$remission=$_POST['remission'];
$age=$_POST['age'];
$Gender=$_POST['Gender'];
// $username=$_POST['username'];
$patient_id=$_POST['patient_id'];
$duration = $_POST['duration'];
$hypertension = $_POST['hypertension'];
$thyroid = $_POST['thyroid'];
$diabetes = $_POST['diabetes'];
$tjc28 = $_POST['tjc28'];
$sjc28 = $_POST['sjc28'];
$tjc68 = $_POST['tjc68'];
$sjc68 = $_POST['sjc68'];
$rf = $_POST['rf'];
$ana = $_POST['ana'];
$esr = $_POST['esr'];
$crp = $_POST['crp'];
$Neutrophil=$_POST['Neutrophil'];
$Lymphocytes = $_POST['Lymphocytes'];
$NLR = $_POST['NLR'];
$DASB = $_POST['DASB'];
$dmards = $_POST['dmards'];
$username=$_SESSION['login'];
$is_active=0;
$query=mysqli_query($con,"insert into rdata(remission,age,gender,patient_id,username,
duration,hypertension,diabetes,thyroid,tjc28,sjc28,tjc68,sjc68,rf,ana,esr,
crp,neutrophil,lymphocytes,nlr,das_baseline,dmards) 
values('$remission','$age','$Gender','$patient_id','$username','$duration','$hypertension',
'$diabetes','$thyroid','$tjc28','$sjc28','$tjc68','$sjc68','$rf',
'$ana','$esr','$crp','$Neutrophil','$Lymphocytes','$NLR','$DASB','$dmards')");

if($query)
{
    echo '<script>alert("Submitted")</script>';
 }
else{
    echo '<script>alert("Failed")</script>'; }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">

    <!-- FontAwesome JS-->
    <script defer src="assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="assets/css/theme.css">
    <style>
    select,
    input[type=text] {
        width: 100%;
        background: ;
        margin-left: auto;
        margin-right: auto;
    }

    form {
        background: #152b1f;
        color: white;
    }

    tr td {
        padding: 10px;
    }

    .check {
        background: #28b76b;
        color: white;
        border: 2px solid #ffffff;
    }
    </style>
</head>

<body>
    <?php include('layout/header.php');?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 check">
                <br>
                <?php include('layout/sidebar.php'); ?>
            </div>
            <div class="col-md-6 check">
                <br>
                <form name="addpost" method="post" enctype="multipart/form-data">
                    <div class="">
                        <h3 class="m-t-10 m-b-10" align="center"
                            style="background-color: #eee;width: 100%;box-shadow: 10px;font-family: times;line-height: 50px;">
                            <b>INSERTION FORM</b></h3>
                    </div>
                    <table>
                        <tr>
                            <td>Remission Achieved</td>
                            <td>
                                <select name="remission" id="remission">
                                    <option value="">Choose Option</option>
                                    <option value="3">3 Months</option>
                                    <option value="12">12 Months</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Age(In Years)</td>
                            <td><input type="text" class="" id="age" name="age" required></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>
                            <select name="Gender" id="Gender">
                                    <option value="">Choose Option</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                    <option value="Other">Other</option>
                                </select> 
                            <!-- <input type="text" class="" id="Gender" name="Gender" required></td> -->
                        </tr>
                        <tr>
                            <td></td>
                            <td><input type="text" class="" id="username" name="username"
                                    value="<?php echo $_SESSION['login']; ?>" required hidden></td>
                        </tr>
                        <tr>
                            <td>Patient Id</td>
                            <td><input type="text" class="" id="patient_id" name="patient_id" required></td>
                        </tr>
                        <tr>
                            <td>Duration of Illness (in months)</td>
                            <td><input type="text" class="" id="duration" name="duration" required></td>
                        </tr>
                        <tr>
                            <td>Hypertension</td>
                            <td>
                                <select name="hypertension" id="hypertension">
                                    <option value="">Choose Option</option>
                                    <option value="1">Present</option>
                                    <option value="0">Not Present</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>Diabetes</td>
                            <td>
                                <select name="diabetes" id="diabetes">
                                    <option value="">Choose Option</option>
                                    <option value="1">Present</option>
                                    <option value="0">Not Present</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Thyroid</td>
                            <td>
                                <select name="thyroid" id="thyroid">
                                    <option value="">Choose Option</option>
                                    <option value="1">Present</option>
                                    <option value="0">Not Present</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>TJC 28</td>
                            <td><input type="text" class="" id="tjc28" name="tjc28" required></td>
                        </tr>
                        <tr>
                            <td>SJC 28</td>
                            <td><input type="text" class="" id="sjc28" name="sjc28" required></td>
                        </tr>
                        <tr>
                            <td>TJC 68</td>
                            <td><input type="text" class="" id="tjc68" name="tjc68" required></td>
                        </tr>
                        <tr>
                            <td>SJC 68</td>
                            <td><input type="text" class="" id="sjc68" name="sjc68" required></td>
                        </tr>
                        <tr>
                            <td>Rheumatoid Factor</td>
                            <td>
                                <select name="rf" id="rf">
                                    <option value="">Choose Option</option>
                                    <option value="1">Present</option>
                                    <option value="0">Not Present</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>ANA</td>
                            <td>
                                <select name="ana" id="ana">
                                    <option value="">Choose Option</option>
                                    <option value="1">Present</option>
                                    <option value="0">Not Present</option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>ESR(mm/hr)</td>
                            <td><input type="text" class="" id="esr" name="esr" required></td>
                        </tr>
                        <tr>
                            <td>CRP(mg/L)</td>
                            <td><input type="text" class="" id="crp" name="crp" required></td>
                        </tr>
                        <tr>
                            <td>Neutrophil (%)</td>
                            <td><input type="text" class="" id="Neutrophil" name="Neutrophil" required></td>
                        </tr>
                        <tr>
                            <td>Lymphocytes (%)</td>
                            <td><input type="text" class=""   id="Lymphocytes" name="Lymphocytes" required onkeyup="nlr()"></td>
                        </tr>
                        <tr>
                            <td>NLR</td>
                            <td><input type="text" class="" id="NLR" name="NLR" required></td>
                        </tr>
                        <tr>
                            <td>DAS 28 at Baseline</td>
                            <td><input type="text" class="" id="DASB" name="DASB" required></td>
                        </tr>
						<tr>
                            <td>DMARDs</td>
                            <td>
                            <select name="dmards" id="dmards">
                                    <option value="">Choose Option</option>
                                    <option value="0">DMARDs 1</option>
                                    <option value="1">DMARDs 2</option>
                                    <option value="2">DMARDs >3</option>
                                </select> 
                            <!-- <input type="text" class="" id="Gender" name="Gender" required></td> -->
                        </tr>
                    </table <br>
                    <div align="center">
                        <button type="submit" name="submit"
                            class="btn btn-success waves-effect waves-light">Submit</button>
                        <!-- <button type="button" class="btn btn-danger waves-effect waves-light">Discard</button> -->
                    </div><br>

                </form> <br>
            </div>
            <div class="col-md-4 check">
                <table class="table dt-table">
                    <thead>
                        <tr>
                            <th>
                                PId
                            </th>
                            <th>
                                RF
                            </th>

                            <th>
                                Result
                            </th>
                            <th>
                                Action
                            </th>

                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php 
               $uname=$_SESSION['login'];
                    $query1=mysqli_query($con,"Select * from rdata  where  username ='$uname' ORDER BY id DESC LIMIT 5 ");
         
while($rows=mysqli_fetch_array($query1))
{
                
               ?>
                        <tr>
                            <td><?php echo htmlentities($rows['patient_id']); ?></td>
                            <td><?php echo htmlentities($rows['rf']); ?></td>
                            <td><?php echo htmlentities($rows['result']); ?></td>
                            <td><a href="details.php?nid=<?php echo htmlentities($rows['id']);?>"
                                    class="btn btn-info">view</a></td>

                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>

        </div>
    </div>

    <div class="page-header theme-bg-dark py-5 text-center position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h1 class="page-heading single-col-max mx-auto"></h1>
            <div class="page-intro single-col-max mx-auto"></div>
            <div class="main-search-box pt-3 d-block mx-auto">

            </div>
        </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h3 class="mb-2 text-white mb-3"></h3>
            <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
            <div class="pt-3 text-center">

            </div>
        </div>
    </section>



    <?php include('layout/footer.php');?>

    <!-- Javascript -->
    <script src="assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="assets/plugins/popper.min.js"></script>
    <script src="assets/js/function.js"></script>
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script>
       
       

       
    function nlr() {
        var num1 =parseInt(document.getElementById('Neutrophil').value);
       var num2 =parseInt(document.getElementById('Lymphocytes').value);
        
       z =num2/num1;
       document.getElementById("NLR").value = z.toFixed(2) ;
    }
    </script>
</body>

</html>

<?php } ?>
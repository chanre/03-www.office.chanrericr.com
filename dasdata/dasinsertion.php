
<?php
session_start();
include('../config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{
$patient_id=$_POST['patient_id'];
$Age=$_POST['Age'];
$Gender=$_POST['Gender'];
$Duration_diagnosis=$_POST['Duration_diagnosis'];
$RF=$_POST['RF'];
$TJC=$_POST['TJC'];
$SJC=$_POST['SJC'];
$ESR=$_POST['ESR'];
$CRP=$_POST['CRP'];
$N=$_POST['N'];
$L=$_POST['L'];
$NLR=$_POST['NLR'];
$Family_history=$_POST['Family_history'];
$Smoking_history=$_POST['Smoking_history'];
$username=$_SESSION['login'];
$is_active=0;
$query=mysqli_query($con,"insert into das_table (patient_id,Age,Gender,Duration_diagnosis,RF,TJC,SJC,ESR,CRP,N,L,NLR,Family_history,Smoking_history,username,is_active) values('$patient_id','$Age','$Gender','$Duration_diagnosis','$RF','$TJC','$SJC','$ESR','$CRP','$N','$L','$NLR','$Family_history','$Smoking_history','$username','$is_active')");


if($query)
    {echo '<script>alert("Submitted")</script>'; }
else{echo '<script>alert("Failed")</script>'; }
}
?>

<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>DAS Data Insertion</title>
    
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
	select{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;}
</style>
</head> 

<body>    
    <?php include('../layout/dasheader.php');?>
    <div class="container-fluid">   
    <div class="row">   
            <div class="col-md-2 check">
                <br>
               <?php include('../layout/sidebar.php'); ?>
            </div>
            <div class="col-md-6 check">
                <br>
            <form name="addpost" method="post" enctype="multipart/form-data">
			  <div class="">
<h3 class="m-t-10 m-b-10" align="center" style="background-color: #eee;width: 100%;box-shadow: 10px;font-family: times;line-height: 50px;"><b>DAS Classified</b></h3>
</div>
 <div class="row">
    <div class="col-sm-3"></div>
<div class="col-sm-6">
  
<div class="">
<label class="m-t-0 "><b>Patient ID</b></label>
<input type="text" class="" id="patient_id" name="patient_id"  required>
</div>

<div class="">
<label class="m-t-0 "><b>Age</b></label>
<input type="text" class="" id="Age" name="Age"  required>
</div>
 
 <div class="">
<label class="m-t-0 "><b>Gender</b></label>
<select class="" id="Gender" name="Gender"  required>
<option value="">Select</option>
<option value="0">Male</option>
<option value="1">Female</option>
</select>

</div>
 <div class="">
<label class="m-t-0 "><b>Duration Diagnosis</b></label>
<input type="text" class="" id="Duration_diagnosis" name="Duration_diagnosis"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>RF</b></label>
<input type="text" class="" id="RF" name="RF"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>TJC</b></label>
<input type="text" class="" id="TJC" name="TJC"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>SJC</b></label>
<input type="text" class="" id="SJC" name="SJC"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>ESR</b></label>
<input type="text" class="" id="ESR" name="ESR"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>CRP</b></label>
<input type="text" class="" id="CRP" name="CRP"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>N</b></label>
<input type="text" class="" id="N" name="N"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>L</b></label>
<input type="text" class="" id="L" name="L"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>NLR</b></label>
<input type="text" class="" id="NLR" name="NLR"  required>
</div>
 <div class="">
<label class="m-t-0 "><b>Family History</b></label>

<select class="" id="Family_history" name="Family_history"  required>
<option value="">Select</option>
<option value="1">Yes</option>
<option value="0">No</option>
</select>
</div>
 <div class="">
<label class="m-t-0 "><b>Smoking History</b></label>

<select class="" id="Smoking_history" name="Smoking_history"  required>
<option value="">Select</option>
<option value="1">Yes</option>
<option value="0">No</option>
</select>
</div>

</div>
</div>   

<br>
<div align="center" class="m-t-10 m-b-20">
<button type="submit" name="submit" class="btn btn-success waves-effect waves-light">Submit</button>

</div>

                                        </form> <br>
            </div>
            <div class="col-md-4 check">
            <table class="table dt-table">
                                    <thead>
                                    <tr>
                                    <th>
                                            PId
                                        </th>
                                        <th>
                                        Age
                                        </th>
                                        
                                        <th>
                                        Result
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                        
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
              <?php 
               $uname=$_SESSION['login'];
                    $query1=mysqli_query($con,"Select * from das_table where username='$uname'");
         
while($rows=mysqli_fetch_array($query1))
{
                
               ?>                    
            <tr>
                <td><?php echo htmlentities($rows['patient_id']); ?></td>
                <td><?php echo htmlentities($rows['Age']); ?></td>
                <td><?php echo htmlentities($rows['results']); ?></td>
                <td><a href="details.php?nid=<?php echo htmlentities($rows['id']);?>" class="btn btn-info">Edit</a></td>
            
            </tr>
            <?php } ?>
                                    </tbody>
                                    </table>
            </div>

    </div>  
    </div>  
    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h1 class="page-heading single-col-max mx-auto"></h1>
            <div class="page-intro single-col-max mx-auto"></div>
            <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
        </div>
    </div>

    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
        <div class="theme-bg-shapes-right"></div>
        <div class="theme-bg-shapes-left"></div>
        <div class="container">
            <h3 class="mb-2 text-white mb-3"></h3>
            <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
            <div class="pt-3 text-center">
               
            </div>
        </div>
    </section>

               
    
 <?php include('../layout/footer.php');?>

    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

</body>
</html> 

<?php } ?>
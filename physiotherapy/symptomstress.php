<?php 
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
  if($_GET['action']='del')
{
$postid=intval($_GET['nid']);
$query1=mysqli_query($con,"delete from stresssymptom where id='$postid'");
}
 
 ?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Dashboard</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="../favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">



<style>
body{background-color: skyblue;}
table{background: lightblue;padding: 10px;}
tr{padding: 10px;}
	span{
		font-size:14px;
		font-family:times;
		 
	}
  form{padding: 20px;}
  tr th{width: 10%;background-color:lightgrey;padding: 10px; }
	textarea{}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.js"></script>

</head> 
<body onload="myFunction()">    
 <?php include('physioheader.php'); ?>
    <div class="container-fluid">	
    <div class="row" >	
    	 <div class="col-md-2"></div>
        <div class="col-md-8">
          <div align="right">
            <br>

            <a href="stressinsert.php" class="btn btn-info">Add Patient</a>

          </div>
          <br>
            <table border="2px" align="center">
              <tr>
                <th>Name</th>
                <th>File no</th>
                <th>Gender</th>
                <th>Age</th>
                <th>Action</th>
              </tr>
              <?php $query=mysqli_query($con, "Select * from stresssymptom");
              while($rows=mysqli_fetch_array($query)){
              ?>
              <tr>
                <td><?php echo htmlentities($rows['name']); ?></td>
                <td><?php echo htmlentities($rows['fileno']); ?></td>
                <td><?php echo htmlentities($rows['gender']); ?></td>
                <td><?php echo htmlentities($rows['age']); ?></td>
                <td><a href="symptomedit.php?pid=<?php echo htmlentities($rows['id']); ?>" class="btn btn-primary rounded btn-sm">Edit</a>| 
                  <a href="symptomstress.php?nid=<?php echo htmlentities($rows['id']);?>&&action=del" onclick="return confirm('Do you realy want to delete ?')" class="btn btn-danger">Del</a></td>

              </tr>
            <?php } ?>
            </table>
        </div>
        <div class="col-md-3"></div>
    </div>	
    </div>	    

      <script>

function myFunction() {
  var greeting;
  var time = new Date().getHours();
  if (time < 12) {
    greeting = "Good Morning";
  } else if (time < 17) {
    greeting = "Good Afternoon";
  } else {
    greeting = "Good Evening";
  }
  document.getElementById("demo").innerHTML = greeting;
}
</script>  
 <footer class="footer">

<div class="footer-bottom text-center py-5">
    
    <ul class="social-list list-unstyled pb-4 mb-0">
        <li class="list-inline-item"><a href="#"><i class="fab fa-github fa-fw"></i></a></li> 
        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-fw"></i></a></li>
    </ul>
    <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i></small>
    
    
</div>

</footer>    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

</body>
</html> 

<?php } ?>
<?php

session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{
if(isset($_POST['submit']))
{

$name=$_POST['name'];
$fileno=$_POST['fileno'];
$diagnosis=$_POST['diagnosis'];
$gender=$_POST['gender'];
$age=$_POST['age'];
$calm=$_POST['calm'];
$secure=$_POST['secure'];
$tense=$_POST['tense'];
$strained=$_POST['strained'];
$ease=$_POST['ease'];
$upset=$_POST['upset'];
$misfortune=$_POST['misfortune'];
$satisfied=$_POST['satisfied'];
$frightened=$_POST['frightened'];
$confident=$_POST['confident'];
$nervous=$_POST['nervous'];
$jittery=$_POST['jittery'];
$indecisive=$_POST['indecisive'];
$relaxed=$_POST['relaxed'];
$content=$_POST['content'];
$worried=$_POST['worried'];
$confused=$_POST['confused'];
$steady=$_POST['steady'];
$pleasant=$_POST['pleasant'];
$result=$_POST['result'];
$feedback=$_POST['feedback'];

$query=mysqli_query($con,"INSERT INTO staiform(name,fileno,diagnosis,gender,age,calm,secure,tense,strained,ease,upset,
  misfortune,satisfied,frightened,confident,nervous,jittery,indecisive,relaxed,content,worried,confused,steady,pleasant,result,feedback)
  values('$name','$fileno','$diagnosis','$gender','$age','$calm','$secure','$tense','$strained','$ease','$upset','$misfortune','$satisfied','$frightened','$confident','$nervous','$jittery','$indecisive','$relaxed','$content','$worried','$confused','$steady','$pleasant','$result','$feedback')");
if($query)
  {echo "<script>alert('Successfully saved..');</script>";
}
else{echo "<script>alert('Unable to save the data..Try again');</script>";
}
}
?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Dashboard</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="../favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    body{background-color: skyblue;}

    form{background: lightblue;padding: 20px;}
    
    span{
        font-size:14px;
        font-family:times;
         
    }

</style>
</head> 
<body onload="myFunction()">    
 <?php include('physioheader.php'); ?>
    <div class="container-fluid">   
    <div class="row" >  
        <div class="col-md-3"></div>
        <div class="col-md-6">
             
                   <form method="post">
                    <h2 align="center">STAI Form Y - 1 </h2>
                    <hr>
           <table align="center">
           <tr> 
           <td>Name:</td>
           <td><input type="text" name="name"></td>
           </tr>
            <tr> 
           <td>File No:</td>
           <td><input type="text" name="fileno"></td>
           </tr>

              <tr> 
           <td>Diagnosis:</td>
           <td><select id="diagnosis" name="diagnosis"> 
            <option>Select option..</option>
            <option value="Ankylosing spondylitis">Ankylosing spondylitis</option>
            <option value="Rheumatoids Arithritis">Rheumatoids Arithritis</option>
            <option value="psoriatic arthritis">psoriatic arthritis</option>
            <option value="Osteo Arithritis">Osteo Arithritis</option>
            <option value="Scleroderma">Scleroderma</option>
            <option value="Fibromyalgia">Fibromyalgia</option>
           </select></td>
           </tr>

            <tr> 
           <td>Age:</td>
           <td><input type="text" name="age"></td>
           </tr>
            <tr> 
           <td>Gender:</td>
           <td><input type="text" name="gender"></td>
           </tr>
           </table>
           <table border="1">
              <tr>
           <td>I feel calm</td>
           <td><select name="calm" id="calm" onchange="pcs()">
           <option value="">Select the below option</option>
            <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
           
           <tr>
           <td>I feel secure</td>
           <td><select name="secure" id="secure" onchange="pcs()">
               <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
        
           <tr>
           <td>I am tense</td>
           <td><select name="tense" id="tense" onchange="pcs()">
            <option value="">Select the below option</option>
               <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel strained</td>
           <td><select name="strained" id="strained" onchange="pcs()">
            <option value="">Select the below option</option>
               <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
           
           
           <tr>
           <td>I feel at ease</td>
           <td><select name="ease" id="ease" onchange="pcs()">
               <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel upset</td>
           <td><select name="upset" id="upset" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
           <tr>
            <td>I'm presently worrying over possible misfortunes.</td>
           <td><select name="misfortune" id="misfortune" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel satisfied</td>
           <td><select name="satisfied" id="satisfied" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel frightened</td>
           <td><select name="frightened" id="frightened" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel comfortable</td>
           <td><select name="comfortable" id="comfortable" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel self-confident</td>
           <td><select name="confident" id="confident" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel nervous</td>
           <td><select name="nervous" id="nervous" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
           
           <tr>
           <td>I am jittery</td>
           <td><select name="jittery" id="jittery" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
            <tr>
           <td>I feel indecisive</td>
           <td><select name="indecisive" id="indecisive" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I am relaxed</td>
           <td><select name="relaxed" id="relaxed" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel content</td>
           <td><select name="content" id="content" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
           
           <tr>
           <td>I am worried</td>
           <td><select name="worried" id="worried" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
          
         
           <tr>
           <td>I feel Confused</td>
           <td><select name="confused" id="confused" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="1">Not at all</option>
             <option value="2">Somewhat</option>
             <option value="3">Moderately So</option>
             <option value="4">Very much So</option>
             </select></td>
           </tr>
           <tr>
           <td>I feel steady</td>
           <td><select name="steady" id="steady" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>
           
           <tr>
           <td>I feel pleasant</td>
           <td><select name="pleasant" id="pleasant" onchange="pcs()">
           <option value="">Select the below option</option>
             <option value="4">Not at all</option>
             <option value="3">Somewhat</option>
             <option value="2">Moderately So</option>
             <option value="1">Very much So</option>
             </select></td>
           </tr>


           </table>
           <div align="center">
           <br>
           <button align="center" onclick="pcs()" type="submit"  name="submit" class="btn btn-primary">Submit</button><br><br>
           <input type="number" id="mex" name="result" readonly> &nbsp;
<br>
           <br>
           <textarea rows="5" name="feedback" class="form-control" placeholder="Enter your text.."></textarea><br>
           <br>
           </div>
           
                   </form>

        </div>
        <div class="col-md-3"></div>
    </div>  
    </div>      

    
      <script>
function myFunction() {
  var greeting;
  var time = new Date().getHours();
  if (time < 12) {
    greeting = "Good Morning";
  } else if (time < 17) {
    greeting = "Good Afternoon";
  } else {
    greeting = "Good Evening";
  }
  document.getElementById("demo").innerHTML = greeting;
}
</script>  
 <footer class="footer">

<div class="footer-bottom text-center py-5">
    
    <ul class="social-list list-unstyled pb-4 mb-0">
        <li class="list-inline-item"><a href="#"><i class="fab fa-github fa-fw"></i></a></li> 
        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-fw"></i></a></li>
    </ul>
    <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i></small>
    
    
</div>

</footer>    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>
  <script>
  function pcs(){
    
   
    const f =Number(document.getElementById("calm").value);
    const g=Number(document.getElementById("secure").value);
  const h=Number(document.getElementById("tense").value);
    const i=Number(document.getElementById("strained").value);
   const j=Number(document.getElementById("ease").value);
    const k=Number(document.getElementById("upset").value);
    const l=Number(document.getElementById("misfortune").value);
    const m=Number(document.getElementById("satisfied").value);
    const n=Number(document.getElementById("frightened").value);
   const o=Number(document.getElementById("confident").value);
    const p=Number(document.getElementById("nervous").value);
    const q=Number(document.getElementById("jittery").value);
    const r=Number(document.getElementById("indecisive").value);
    const s=Number(document.getElementById("relaxed").value);
    const t=Number(document.getElementById("content").value);
    const u=Number(document.getElementById("worried").value);
    const v=Number(document.getElementById("confused").value);
    const w=Number(document.getElementById("steady").value);
    const x=Number(document.getElementById("pleasant").value);
    // const result=Number(document.getElementById("result").value);
    // const feedback=Number(document.getElementById("feedback").value);

    
      const res= f+g+h+i+j+k+l+m+n+o+p+q+r+s+t+u+v+w+x;
  
  document.getElementById("mex").value= res;
  }
  </script>
</body>
</html> 

<?php   } ?>
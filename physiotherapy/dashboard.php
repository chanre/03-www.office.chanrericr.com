<?php   session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{ ?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Dashboard</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    form{background:#152b1f;color:white;}
    tr td{padding:10px;}
    .check{background:#28b76b;color:white;border: 2px solid #ffffff;padding:40px;}
	.side-item{
		background-color: white;
		margin: 30px;
		padding: 10px;
	}
	.side-item:hover{
		background-color: black;
		color:white;
		margin: 30px;
		padding: 10px;
	}
	.side-item a{
		width: 100%;
	}
	span{
		font-size:14px;
		font-family:times;
		 
	}
	span::first-letter {
 text-transform: uppercase;
}
	div a{
		font-size:14px;
	}
</style>
</head> 
<body onload="myFunction()">    
   <?php include('physioheader.php'); ?>
    <div class="container-fluid">	
    <div class="row" >	
    		<div class="col-md-3 check">
                <div align="center">
				<img src="../images/nopics.jpg" width="200" height="200">
				</div>
            </div>
    		<div class="col-md-3 check">
              <div class="side-item">
		<a href="pcs.php"  class="btn btn-primary">PCS</a>
	</div>
	<div class="side-item">
		<a href="stai1.php"  class="btn btn-primary">STAI Y-1</a>
	</div>
    		</div>
			<div class="col-md-3 check">
              <div class="side-item">
		<a href="stai2.php"  class="btn btn-primary">STAI Y-2</a>
	</div>
	
	<div class="side-item">
		<a href="symptomstress.php"  class="btn btn-primary">Symptoms Of Stress</a>
	</div>
    		</div>
    		<div class="col-md-3 check">
        <!--    <div class="side-item">
		 <a href="https://office.chanrericr.com/board/add_article.php"  class="btn btn-primary">Posts And Articles</a> 
	</div>-->
            </div>
    </div>	
    </div>	    
    <div class="page-header theme-bg-dark py-5 text-center position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h1 class="page-heading single-col-max mx-auto"></h1>
		    <div class="page-intro single-col-max mx-auto"></div>
		    <div class="main-search-box pt-3 d-block mx-auto">
                
             </div>
	    </div>
    </div>
    <section class="cta-section text-center py-5 theme-bg-dark position-relative">
	    <div class="theme-bg-shapes-right"></div>
	    <div class="theme-bg-shapes-left"></div>
	    <div class="container">
		    <h3 class="mb-2 text-white mb-3"></h3>
		    <div class="section-intro text-white mb-3 single-col-max mx-auto"></div>
		    <div class="pt-3 text-center">
			   
		    </div>
	    </div>
    </section>
      <script>
function myFunction() {
  var greeting;
  var time = new Date().getHours();
  if (time < 12) {
    greeting = "Good Morning";
  } else if (time < 17) {
    greeting = "Good Afternoon";
  } else {
    greeting = "Good Evening";
  }
  document.getElementById("demo").innerHTML = greeting;
}
</script>  
 <footer class="footer">

<div class="footer-bottom text-center py-5">
    
    <ul class="social-list list-unstyled pb-4 mb-0">
        <li class="list-inline-item"><a href="#"><i class="fab fa-github fa-fw"></i></a></li> 
        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-fw"></i></a></li>
    </ul>
    <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i></small>
    
    
</div>

</footer>    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  

</body>
</html> 

<?php   } ?>
<?php 
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

if(isset($_POST['submit']))
{
$name= $_POST['name'];
$fileno=$_POST['fileno'];
$diagnosis=$_POST['diagnosis'];
$gender=$_POST['gender'];
$age=$_POST['age'];
$pleasant=$_POST['pleasant'];
$nervous=$_POST['nervous'];
$satisfied=$_POST['satisfied'];
$wish=$_POST['wish'];
$failure=$_POST['failure'];
$rested=$_POST['rested'];
$calm=$_POST['calm'];
$overcome=$_POST['overcome'];
$something=$_POST['something'];
$happy=$_POST['happy'];
$disturbing=$_POST['disturbing'];
$confidence=$_POST['confidence'];
$secure=$_POST['secure'];
$decisions=$_POST['decisions'];
$inadequate=$_POST['inadequate'];
$content=$_POST['content'];
$thoughts=$_POST['thoughts'];
$disappointment=$_POST['disappointment'];
$steady=$_POST['steady'];
$interests=$_POST['interests'];
$result=$_POST['result'];
$feedback= $_POST['feedback'];
$query=mysqli_query($con,"Insert into staiform_2(name,fileno,diagnosis,age,gender,pleasant,nervous,satisfied,wish,failure,rested,calm,overcome,something,happy,disturbing,confidence,secure,decisions,inadequate,content,thoughts,disappointment,steady,interests,result,feedback)
values('$name','$fileno','$diagnosis','$age','$gender','$pleasant','$nervous','$satisfied','$wish','$failure','$rested','$calm','$overcome','$something','$happy','$disturbing','$confidence','$secure','$decisions','$inadequate','$content','$thoughts','$disappointment','$steady','$interests','$result','$feedback')
  ");

if($query)
  {echo "<script>alert('Successfully saved..');</script>";
}
else{echo "<script>alert('Unable to save the data..Try again');</script>";
}

}


 ?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Dashboard</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="../favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    body{background-color: skyblue;}

    form{background: #6bce9a;padding: 20px;}
    
  span{
    font-size:14px;
    font-family:times;
     
  }
  
  textarea{f: 10px;}
</style>
</head> 
<body onload="myFunction()">    
 <?php include('physioheader.php'); ?>
    <div class="container-fluid"> 
    <div class="row" >  
      <div class="col-md-3"></div>
        <div class="col-md-6">
             <form method="post"><h2 align="center">STAI Form Y-2</h2>
                   <table align="center">
                   <tr> 
                   <td>Name:</td>
                   <td><input type="text" name="name"></td>
                   </tr>
                    <tr> 
                   <td>File No:</td>
                   <td><input type="text" name="fileno"></td>
                   </tr>

                      <tr> 
                   <td>Diagnosis:</td>
                   <td><select id="diagnosis" name="diagnosis"> 
                    <option>Select option..</option>
                    <option value="Ankylosing spondylitis">Ankylosing spondylitis</option>
                    <option value="Rheumatoids Arithritis">Rheumatoids Arithritis</option>
                    <option value="psoriatic arthritis">psoriatic arthritis</option>
                    <option value="Osteo Arithritis">Osteo Arithritis</option>
                    <option value="Scleroderma">Scleroderma</option>
                    <option value="Fibromyalgia">Fibromyalgia</option>
                   </select></td>
                   </tr>

                    <tr> 
                   <td>Age:</td>
                   <td><input type="text" name="age"></td>
                   </tr>
                    <tr> 
                   <td>Gender:</td>
                   <td><input type="text" name="gender"></td>
                   </tr>
                   </table>
                   <table border="1">
                   <tr>
                   <td>I feel pleasant</td>
                   <td><select name="pleasant" id="pleasant" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       
                       </select></td>
                   </tr>
                   <tr>
                   <td>I feel nervous and restless</td>
                   <td><select name="nervous" id="nervous" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I feel satisfied with myself</td>
                   <td><select name="satisfied" id="satisfied" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I wish I could be as happy as others seems to be</td>
                   <td><select name="wish" id="wish" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I feel like a failure</td>
                   <td><select name="failure" id="failure" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I feel rested</td>
                   <td><select name="rested" id="rested" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I'm "calm,coola and collected"</td>
                   <td><select name="calm" id="calm" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I feel the difficulties are piling up so that I can not overcome</td>
                   <td><select name="overcome" id="overcome" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I worry too much over something that really doesn't matter</td>
                   <td><select name="something" id="something" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I am Happy</td>
                   <td><select name="happy" id="happy" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I have disturbing thoughts</td>
                   <td><select name="disturbing" id="disturbing" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I lack self-confidence</td>
                   <td><select name="confidence" id="confidence" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   
                   <tr>
                   <td>I feel secure</td>
                   <td><select name="secure" id="secure" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I make decisions easily</td>
                   <td><select name="decisions" id="decisions" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I feel inadequate</td>
                   <td><select name="inadequate" id="inadequate" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I am content</td>
                   <td><select name="content" id="content" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       </select></td>
                   </tr>
                    <tr>
                   <td>Some unimportant thoughts runs through my mind and bothers me </td>
                   <td><select name="thoughts" id="thoughts" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                    <tr>
                   <td>I take disappointment so keenly that I can't put them out of my mind</td>
                   <td><select name="disappointment" id="disappointment" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                    <tr>
                   <td>I am a steady person</td>
                   <td><select name="steady" id="steady" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="4">Almost Never</option>
                       <option value="3">Sometimes</option>
                       <option value="2">Often</option>
                       <option value="1">Almost Always</option>
                       </select></td>
                   </tr>
                    <tr>
                   <td>I get in a state of tension or turmoil as I think over my recent concerns and interests</td>
                   <td><select name="interests" id="interests" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="1">Almost Never</option>
                       <option value="2">Sometimes</option>
                       <option value="3">Often</option>
                       <option value="4">Almost Always</option>
                       </select></td>
                   </tr>
                   </table>
                   <div align="center">
                   <br>
                   <button align="center" onclick="pcs()" type="submit"  name="submit" class="btn btn-primary">Submit</button><br><br>
                   <input type="number" id="mex" name="result" readonly> &nbsp;
<br>
                   <br>
                   <textarea rows="5" name="feedback" class="form-control" placeholder="Enter your text.."></textarea><br>
                   <br>
                   </div>
                   
                   </form>

        </div>
        <div class="col-md-3"></div>
    </div>  
    </div>      

    
      <script>
function myFunction() {
  var greeting;
  var time = new Date().getHours();
  if (time < 12) {
    greeting = "Good Morning";
  } else if (time < 17) {
    greeting = "Good Afternoon";
  } else {
    greeting = "Good Evening";
  }
  document.getElementById("demo").innerHTML = greeting;
}
</script>  
 <footer class="footer">

<div class="footer-bottom text-center py-5">
    
    <ul class="social-list list-unstyled pb-4 mb-0">
        <li class="list-inline-item"><a href="#"><i class="fab fa-github fa-fw"></i></a></li> 
        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-fw"></i></a></li>
    </ul>
    <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i></small>
    
    
</div>

</footer>    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>
  <script>
  function pcs(){
    const pleasant=Number(document.getElementById("pleasant").value);
    const nervous=Number(document.getElementById("nervous").value);
    const satisfied=Number(document.getElementById("satisfied").value);
    const wish=Number(document.getElementById("wish").value);
    const failure=Number(document.getElementById("failure").value);
    const rested=Number(document.getElementById("rested").value);
    const calm=Number(document.getElementById("calm").value);
    const overcome=Number(document.getElementById("overcome").value);
    const something=Number(document.getElementById("something").value);
    const happy=Number(document.getElementById("happy").value);
    const disturbing=Number(document.getElementById("disturbing").value);
    const confidence=Number(document.getElementById("confidence").value);
    const secure=Number(document.getElementById("secure").value);
    const decisions=Number(document.getElementById("decisions").value);
    const inadequate=Number(document.getElementById("inadequate").value);
    const content=Number(document.getElementById("content").value);
    const thoughts=Number(document.getElementById("thoughts").value);
    const disappointment=Number(document.getElementById("disappointment").value);
    const steady=Number(document.getElementById("steady").value);
    const interests=Number(document.getElementById("interests").value);
   
    
    
      const n = pleasant+nervous+satisfied+wish+failure+rested+calm+overcome+something+happy+disturbing+confidence+secure+decisions+inadequate+content+thoughts+disappointment+steady+interests;
  
  document.getElementById("mex").value= n.toFixed(0);
  }
  </script>
</body>
</html> 
<?php   } ?>

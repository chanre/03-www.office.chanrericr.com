<?php 
session_start();
include('config.php');
error_reporting(0);
if(strlen($_SESSION['login'])==0)
  { 
header('location:index.php');
}
else{

if(isset($_POST['submit']))
{
$name= $_POST['name'];
$fileno=$_POST['fileno'];
$diagnosis=$_POST['diagnosis'];
$gender=$_POST['gender'];
$age=$_POST['age'];
$a = $_POST['a'];
$b = $_POST['b'];
$c = $_POST['c'];
$d = $_POST['d'];
$e = $_POST['e'];
$f = $_POST['f'];
$g = $_POST['g'];
$h = $_POST['h'];
$i = $_POST['i'];
$j = $_POST['j'];
$k = $_POST['k'];
$l = $_POST['l'];
$m = $_POST['m'];
$mex = $_POST['mex'];
$feedback= $_POST['feedback'];
$query=mysqli_query($con,"Insert into pcs (name,fileno,diagnosis,age,gender,a,b,c,d,e,f,g,h,i,j,k,l,m,total,feedback)
values('$name','$fileno','$diagnosis','$age','$gender','$a','$b','$c','$d','$e','$f','$g','$h','$i','$j','$k','$l','$m','$mex','$feedback')
  ");

if($query)
  {echo "<script>alert('Successfully saved..');</script>";
}
else{echo "<script>alert('Unable to save the data..Try again');</script>";
}

}


 ?>
<!DOCTYPE html>
<html lang="en"> 
<head>
    <title>Dashboard</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">    
    <link rel="shortcut icon" href="../favicon.ico"> 
    
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&display=swap" rel="stylesheet">
    
    <!-- FontAwesome JS-->
    <script defer src="../assets/fontawesome/js/all.min.js"></script>

    <!-- Theme CSS -->  
    <link id="theme-style" rel="stylesheet" href="../assets/css/theme.css">
<style>
    input[type=text]{
        width:100%;
        background:;
        margin-left:auto;
        margin-right:auto;
    }
    body{background-color: skyblue;}

    form{background: #6bce9a;padding: 20px;}
    
	span{
		font-size:14px;
		font-family:times;
		 
	}
  
	textarea{f: 10px;}
</style>
</head> 
<body onload="myFunction()">    
 <?php include('physioheader.php'); ?>
    <div class="container-fluid">	
    <div class="row" >	
    	<div class="col-md-3"></div>
        <div class="col-md-6">
             <form method="post"><h2 align="center">Pain Catastrophizing Scale</h2>
                   <table align="center">
                   <tr> 
                   <td>Name:</td>
                   <td><input type="text" name="name"></td>
                   </tr>
                    <tr> 
                   <td>File No:</td>
                   <td><input type="text" name="fileno"></td>
                   </tr>

                      <tr> 
                   <td>Diagnosis:</td>
                   <td><select id="diagnosis" name="diagnosis"> 
                    <option>Select option..</option>
                    <option value="Ankylosing spondylitis">Ankylosing spondylitis</option>
                    <option value="Rheumatoids Arithritis">Rheumatoids Arithritis</option>
                    <option value="psoriatic arthritis">psoriatic arthritis</option>
                    <option value="Osteo Arithritis">Osteo Arithritis</option>
                    <option value="Scleroderma">Scleroderma</option>
                    <option value="Fibromyalgia">Fibromyalgia</option>
                   </select></td>
                   </tr>

                    <tr> 
                   <td>Age:</td>
                   <td><input type="text" name="age"></td>
                   </tr>
                    <tr> 
                   <td>Gender:</td>
                   <td><input type="text" name="gender"></td>
                   </tr>
                   </table>
                   <table border="1">
                   <tr>
                   <td>I worry all the time about whether the pain will end</td>
                   <td><select name="a" id="a" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I feel I can’t go on</td>
                   <td><select name="b" id="b" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>It’s terrible and I think it’s never going to get any better</td>
                   <td><select name="c" id="c" onchange="pcs()">
                       <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>It’s awful and I feel that it overwhelms me</td>
                   <td><select name="d" id="d" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I feel I can’t stand it anymore</td>
                   <td><select name="e" id="e" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I become afraid that the pain will get worse</td>
                   <td><select name="f" id="f" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I keep thinking of other painful events</td>
                   <td><select name="g" id="g" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I anxiously want the pain to go away</td>
                   <td><select name="h" id="h" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I can’t seem to keep it out of my mind</td>
                   <td><select name="i" id="i" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I keep thinking about how much it hurts</td>
                   <td><select name="j" id="j" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>I keep thinking about how badly I want the pain to stop</td>
                   <td><select name="k" id="k" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   <tr>
                   <td>There’s nothing I can do to reduce the intensity of the pain</td>
                   <td><select name="l" id="l" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   
                   <tr>
                   <td>I wonder whether something serious may happen</td>
                   <td><select name="m" id="m" onchange="pcs()">
                   <option value="">Select</option>
                       <option value="0">Not at all</option>
                       <option value="1">To aslight degree</option>
                       <option value="2">To a moderate degree</option>
                       <option value="3">To a great degree</option>
                       <option value="4">All the time</option>
                       </select></td>
                   </tr>
                   </table>
                   <div align="center">
                   <br>
                   <button align="center" onclick="pcs()" type="submit"  name="submit" class="btn btn-primary">Submit</button><br><br>
                   <input type="number" id="mex" name="mex" readonly> &nbsp;
<br>
                   <br>
                   <textarea rows="5" name="feedback" class="form-control" placeholder="Enter your text.."></textarea><br>
                   <br>
                   </div>
                   
                   </form>

        </div>
        <div class="col-md-3"></div>
    </div>	
    </div>	    

    
      <script>
function myFunction() {
  var greeting;
  var time = new Date().getHours();
  if (time < 12) {
    greeting = "Good Morning";
  } else if (time < 17) {
    greeting = "Good Afternoon";
  } else {
    greeting = "Good Evening";
  }
  document.getElementById("demo").innerHTML = greeting;
}
</script>  
 <footer class="footer">

<div class="footer-bottom text-center py-5">
    
    <ul class="social-list list-unstyled pb-4 mb-0">
        <li class="list-inline-item"><a href="#"><i class="fab fa-github fa-fw"></i></a></li> 
        <li class="list-inline-item"><a href="#"><i class="fab fa-twitter fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-slack fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-product-hunt fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-facebook-f fa-fw"></i></a></li>
        <li class="list-inline-item"><a href="#"><i class="fab fa-instagram fa-fw"></i></a></li>
    </ul>
    <small class="copyright">Designed with <i class="fas fa-heart" style="color: #fb866a;"></i></small>
    
    
</div>

</footer>    <!-- Javascript -->          
    <script src="../assets/plugins/jquery-3.4.1.min.js"></script>
    <script src="../assets/plugins/popper.min.js"></script>
    <script src="../assets/js/function.js"></script>
    <script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>  
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5f27d8339495c400"></script>    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <script src="js/custom.js"></script>
  <script>
  function pcs(){
    const a=Number(document.getElementById("a").value);
    const b=Number(document.getElementById("b").value);
    const c=Number(document.getElementById("c").value);
    const d=Number(document.getElementById("d").value);
    const e=Number(document.getElementById("e").value);
    const f=Number(document.getElementById("f").value);
    const g=Number(document.getElementById("g").value);
    const h=Number(document.getElementById("h").value);
    const i=Number(document.getElementById("i").value);
    const j=Number(document.getElementById("j").value);
    const k=Number(document.getElementById("k").value);
    const l=Number(document.getElementById("l").value);
    const m=Number(document.getElementById("m").value);
    
    
      const n = a+b+c+d+e+f+g+h+i+j+k+l+m;
  
  document.getElementById("mex").value= n.toFixed(0);
  }
  </script>
</body>
</html> 

<?php   } ?>